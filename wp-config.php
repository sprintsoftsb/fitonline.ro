<?php
/**
 * Setările de bază pentru WordPress.
 *
 * Acest fișier este folosit la crearea wp-config.php în timpul procesului de instalare.
 * Folosirea interfeței web nu este obligatorie, acest fișier poate fi copiat
 * sub numele de „wp-config.php”, iar apoi populate toate detaliile.
 *
 * Acest fișier conține următoarele configurări:
 *
 * * setările MySQL
 * * cheile secrete
 * * prefixul pentru tabele
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Setările MySQL: aceste informații pot fi obținute de la serviciile de găzduire ** //
/** Numele bazei de date pentru WordPress */
define( 'DB_NAME', 'fitonline_db' );

/** Numele de utilizator MySQL */
define( 'DB_USER', 'root' );

/** Parola utilizatorului MySQL */
define( 'DB_PASSWORD', '' );

/** Adresa serverului MySQL */
define( 'DB_HOST', 'localhost' );

/** Setul de caractere pentru tabelele din baza de date. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Schema pentru unificare. Nu face modificări dacă nu ești sigur. */
define('DB_COLLATE', '');

/**#@+
 * Cheile unice pentru autentificare
 *
 * Modifică conținutul fiecărei chei pentru o frază unică.
 * Acestea pot fi generate folosind {@link https://api.wordpress.org/secret-key/1.1/salt/ serviciul pentru chei de pe WordPress.org}
 * Pentru a invalida toate cookie-urile poți schimba aceste valori în orice moment. Aceasta va forța toți utilizatorii să se autentifice din nou.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7Gu|f.f^Ogmv/p;]Ey7/eOpbCKT&N}vb&h-nQ b4mZjti|d;`$X9=cq0U28*g!-~' );
define( 'SECURE_AUTH_KEY',  'DtO&@0P74e+IOcFnrSXrp./x{QICMU$8YExiTqUd]p[_w1c36[iIL2Pm-N/2nju0' );
define( 'LOGGED_IN_KEY',    '(9C <Dm2^+V$7~f*hOhq@r1@0~xSai+8rSJ60[$, 34j3j7(j4-e?xy_W{$. <0A' );
define( 'NONCE_KEY',        'Pc<wf8>)oRre;|k~nClA|X36:Dwgy=lvqXd3x({jYf|sD)s3wp=wa;Eyq)PF??V!' );
define( 'AUTH_SALT',        '?}iLni0YaN!@48V$VRcMI[%CTF9:dH+`{1IwbQH=;f)dX}%ua[?LcqBJ4R/-2]pb' );
define( 'SECURE_AUTH_SALT', ':9~zW[7$lNJsJ-_|2upc?[H!llat7.*Bphn:p FT/n4q,U{H4Jc1lt(+!lX7Avz4' );
define( 'LOGGED_IN_SALT',   '7]syvQRd3Q*|C,w)q_R3[O*DS%s{O6FPY08z; }(ef7vDjC3vs,f]q2&;ZQhlnaY' );
define( 'NONCE_SALT',       '3k?<~@GF~@x]YjK5BgmWGh^}6W3XyFSLnDCVn`iKmi>>AZS&lU$^b]JeBq^Tkt<a' );

/**#@-*/

/**
 * Prefixul tabelelor din MySQL
 *
 * Acest lucru permite instalarea mai multor instanțe WordPress folosind aceeași bază de date
 * dacă prefixul este diferit pentru fiecare instanță. Sunt permise doar cifre, litere și caracterul liniuță de subliniere.
 */
$table_prefix = 'wp_';

/**
 * Pentru dezvoltatori: WordPress în mod de depanare.
 *
 * Setează cu true pentru a permite afișarea notificărilor în timpul dezvoltării.
 * Este recomadată folosirea modului WP_DEBUG în timpul dezvoltării modulelor și
 * a șabloanelor/temelor în mediile personale de dezvoltare.
 *
 * Pentru detalii despre alte constante ce pot fi utilizate în timpul depanării,
 * vizitează Codex-ul.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Asta e tot, am terminat cu editarea. Spor! */

/** Calea absolută spre directorul WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Setează variabilele WordPress și fișierele incluse. */
require_once(ABSPATH . 'wp-settings.php');
