/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * player,'.$live_event_uri_final.','.$live_conect_views.'
 */

window.WebSocket = window.WebSocket || window.MozWebSocket;
if (!window.WebSocket) {
  console.log("Sorry, but your browser doesnt support WebSockets");
}

                  
var is_player_play="no";
var counters_storage=[];
var players_track=[];
var players_start_timer=[];
var timeQueue;
var chat_conected='';

function wpstream_player_initialize(now,live_event_uri_final,live_conect_views){
   
    var player = videojs('wpstream-video'+now,{
            html5: {
            hls: {
                bandwidth: 500000,
                useBandwidthFromLocalStorage: true,
                overrideNative: !videojs.browser.IS_SAFARI,
                smoothQualityChange: true
                }
            },
            errorDisplay: false,
            autoplay:true,
            preload:"auto"
    });
    
    players_track[now]=player;
   
    

}



function wpstream_player_play(now,live_event_uri_final,live_conect_views){
    
    var player = players_track[now];
    
   
    wpstream_player_load(player,live_event_uri_final,live_conect_views);
    
    jQuery("#wpestream_live_counting").appendTo(jQuery('#wpstream-video'+now));
    
    
    
    timeQueue = [];
    var player_start_interval=  setInterval(function(){
        timeQueue.push(player.currentTime());
        if (timeQueue.length > 30){
            timeQueue.shift();
            if (timeQueue[0] == timeQueue[timeQueue.length -1]){

                if (!player.paused() || player.currentTime() == 0){
                    timeQueue = [];
                    try{
                        player.currentTime(0);
                    }catch(err){

                    }
                    wpstream_player_load(player,live_event_uri_final,live_conect_views);
                }

            }
        }
    }, 1000);
    
    players_start_timer[now]=player_start_interval;
}




function wpstream_player_load(player,live_event_uri_final,live_conect_views){
    player.src({
        src:  live_event_uri_final,
        type: "application/x-mpegURL"
    });
    player.play();
    is_player_play="yes";
  
}





function wpstream_count_connect_plugin(player,live_conect_views){
    var connection;
    
    if(typeof(live_conect_views)!=='undefined'){
        connection = new WebSocket(live_conect_views);
        counters_storage[player]=connection;
    }
   

  
    if(typeof(connection)==="undefined"){
        return;
    }  
      
    connection.onopen = function () {
      console.log("connected.");
    };

    connection.onclose = function(){
        console.log("closed. reconnecting...");
        setTimeout(function(){ 
            wpstream_count_connect_plugin(player,live_conect_views) }, 5 * 1000);
    }

    connection.onerror = function (error) {
        console.log("onerror: ", error);
    };

    connection.onmessage = function (message) {
        try {
            var json = JSON.parse(message.data);
        } catch (e) {
            console.log("Invalid JSON: ", message.data);
            return;
        }
        if (json.type === "viewerCount") { 
            count = json.data;
            console.log("viewers: " + count);
            var view_box=jQuery("#"+player+" .wpestream_live_counting");
            view_box.css("background-color","#c91d1d");
            view_box.html( count + " Viewers");

        } else {
            console.log("Unknown type:", json);
        }
    };
}





function wpstream_check_player_status_ticker(player_wrapper,event_id){
    wpstream_interval_code(player_wrapper,event_id);
   
    setInterval(function(){
        wpstream_interval_code(player_wrapper,event_id);
    
    }, 60000);
    
   
}


function wpstream_interval_code(player_wrapper,event_id){
    
     
            var ajaxurl     = wpstream_player_vars.admin_url + 'admin-ajax.php';
        
            jQuery.ajax({
                type: 'POST',
                url: ajaxurl,
                dataType: 'json',
                data: {
                    'action'                    :   'wpstream_player_check_status',
                    'event_id'                  :   event_id,
                },
                success: function (data) {     
            
                    if(data.started=='yes'){
                         chat_conected='yes';
                        var player_id   =   player_wrapper.attr('id');
                        if(is_player_play==="no"){
                                player_wrapper.find('.wpstream_not_live_mess').hide();
                              
                                var now         =   player_wrapper.attr('data-now');
                                // add data.event_uri 
                                counters_storage[player_id]=data.live_conect_views;
                                wpstream_count_connect_plugin(player_id ,data.live_conect_views);
                                wpstream_player_play(now,data.event_uri,data.live_conect_views);
                             
                                if(typeof(connect)==='function' ){
                                    connect(data.chat_url);
                                    
                                }
                                  
                            
                                is_player_play="yes";
                                jQuery("#wpestream_live_counting").show();
                               
                        }
                       
                        if(typeof(counters_storage[player_id]) !=='undefined'){
                            //do nothing
                        }else{
                            wpstream_count_connect_plugin(player_id ,data.live_conect_views);
                        }
               
                       
                    }else if(data.started=='no'){
                        if(is_player_play==="yes"){
                            var now     =player_wrapper.attr('data-now');
                            var player_id   =   player_wrapper.attr('id');
                              
                            player_wrapper.find('.wpstream_not_live_mess').show();
                            is_player_play="no";
                            wpstream_player_stop(now,player_id,event_id);
                            
                            if(typeof(wpstream_close_socket)==='function' ){
                                wpstream_close_socket();
                            }
                            
                            
                            if(typeof(showChat)==='function' && chat_conected==='yes' ){
                                showChat('info', null, wpstream_player_vars.chat_not_connected);
                                chat_conected='no';
                            }
                           
                            
                        }
                      
                       

                         
                    }
                    

                },
                error: function (errorThrown) { 

                }
            });
}





function wpstream_player_stop(now,player_id,event_id){
   var player = players_track[now];

    player.autoplay=false;
    player.pause();
    
    clearInterval( players_start_timer[now]);
    arrayRemove(players_start_timer,now);
    delete  players_start_timer[now];
    
    connection= counters_storage[player_id];
    connection.close();
    delete counters_storage[player_id];
    
    wpstream_plater_close_all_socket_connection();
    jQuery("#wpestream_live_counting").empty().hide();
     
}


function wpstream_plater_close_all_socket_connection(){
    counters_storage.forEach(function(item){
       item.close();
    });
}


function arrayRemove(arr, value) { return arr.filter(function(ele){ return ele != value; });}





jQuery(document).ready(function ($) {
    
  var event_id;  
  var player_wrapper;
  jQuery('.wpstream_live_player_wrapper').each(function(){
      event_id          =   jQuery(this).attr('data-product-id');
      player_wrapper    =   jQuery(this);
    
      wpstream_check_player_status_ticker(player_wrapper,event_id);
  });
    
});


function wpstream_force_clear_transient(event_id){
    var ajaxurl     = wpstream_player_vars.admin_url + 'admin-ajax.php';
            
    jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
   
        data: {
            'action'                    :   'wpstream_force_clear_transient',
            'event_id'                  :   event_id,
        },
        success: function (data) {  
        }, error: function (errorThrown) { 

        }
    });
                
}