<?php
/*
Plugin Name: Progression Theme Elements - SKRN
Text Domain: progression-elements-skrn
Plugin URI: https://progressionstudios.com
Description: Theme Elements for Progression Studios Theme
Version: 1.6
Author: Progression Studios
Author URI: https://progressionstudios.com/
Author Email: contact@progressionstudios.com
License: GNU General Public License v3.0
Text Domain: progression-elements-skrn 
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.


define( 'PROGRESSION_THEME_ELEMENTS_URL', plugins_url( '/', __FILE__ ) );
define( 'PROGRESSION_THEME_ELEMENTS_PATH', plugin_dir_path( __FILE__ ) );


// Translation Setup
add_action('plugins_loaded', 'progression_theme_elements_skrn');
function progression_theme_elements_skrn() {
	load_plugin_textdomain( 'progression-elements-skrn', false, dirname( plugin_basename(__FILE__) ) . '/languages/' );
}

/**
* Enqueue or de-enqueue third party plugin scripts/styles
*/
function progression_skrn_theme_elements_dequeue_styles_scripts() {

	if ( is_singular( 'video_skrn' )) {
		wp_enqueue_style( 'dashicons' ); // Icons for Comment Rating
	}
	
	wp_deregister_script( 'boosted_elements_progression_flexslider_js' ); //Removing a script
	wp_deregister_script( 'boosted_elements_progression_masonry_js' ); //Removing a script
	
	
	wp_register_script( 'owl-carousel', PROGRESSION_THEME_ELEMENTS_URL.'inc/js/owl.carousel.min.js', array( 'jquery' ));
	
	if ( is_user_logged_in() ) {
		wp_enqueue_script( 'owl-carousel' );
	}
	
	
}
add_action( 'wp_enqueue_scripts', 'progression_skrn_theme_elements_dequeue_styles_scripts', 999 );


/**
* Calling new Page Builder Elements
*/
require_once PROGRESSION_THEME_ELEMENTS_PATH.'inc/elementor-helper.php';

function progression_skrn_elements_load_elements(){
	require_once PROGRESSION_THEME_ELEMENTS_PATH.'elements/post-element.php';
	require_once PROGRESSION_THEME_ELEMENTS_PATH.'elements/slider-element.php';
	require_once PROGRESSION_THEME_ELEMENTS_PATH.'elements/carousel-element.php';
}
add_action('elementor/widgets/widgets_registered','progression_skrn_elements_load_elements');


/**
 * Custom Metabox Fields
 */
require PROGRESSION_THEME_ELEMENTS_PATH.'inc/custom-meta.php';


/**
 * Custom Social Sharing
 */
require PROGRESSION_THEME_ELEMENTS_PATH.'inc/social-sharing.php';


/**
 * Comment Rating
 */
require PROGRESSION_THEME_ELEMENTS_PATH.'inc/comment-rating.php';

/**
 * Condition Logic For Custom meta
 */
require PROGRESSION_THEME_ELEMENTS_PATH.'inc/cmb2-conditional-logic.php';



/**
 * Registering Custom Post Type
 */
add_action('init', 'skrn_progression_portfolio_init');
function skrn_progression_portfolio_init() {	
	
//	register_post_type(
//		'notifications_skrn',
//		array(
//			'labels' => array(
//				'name' => esc_html__( "Notifications", "progression-elements-skrn" ),
//				'singular_name' => esc_html__( "Notification Post", "progression-elements-skrn" )
//			),
//			'menu_icon' => 'dashicons-pressthis',
//			'public' => true,
//			'has_archive' => true,
//			'rewrite' => array('slug' => 'notifications_type'),
//			'supports' => array('title', 'thumbnail'),
//			'can_export' => true,
//		)
//	);
	
	register_post_type(
		'video_skrn',
		array(
			'labels' => array(
				'name' => esc_html__( "Videos", "progression-elements-skrn" ),
				'singular_name' => esc_html__( "Video Post", "progression-elements-skrn" )
			),
			'menu_icon' => 'dashicons-format-video',
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'videos'),
			'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'comments'),
			'can_export' => true,
		)
	);
	
	register_taxonomy(
		'video-type', 'video_skrn', 
		array('hierarchical' => true, 
		'label' => esc_html__( "Video Types", "progression-elements-skrn" ), 
		'query_var' => true, 
		'rewrite' => array('slug' => 'media-type'),
		)
	 );
	 
  	register_taxonomy(
  		'video-genres', 'video_skrn', 
  		array('hierarchical' => true, 
  		'label' => esc_html__( "Video Genres", "progression-elements-skrn" ), 
  		'query_var' => true, 
  		'rewrite' => array('slug' => 'media-genre'),
  		)
  	 );

	register_taxonomy(
		'video-category', 'video_skrn', 
		array('hierarchical' => true, 
		'label' => esc_html__( "Video Categories", "progression-elements-skrn" ), 
		'query_var' => true, 
		'rewrite' => array('slug' => 'media-category'),
		)
	 );
	 
	 
 	register_taxonomy(
 		'video-director', 'video_skrn', 
 		array('hierarchical' => true, 
 		'label' => esc_html__( "Director", "progression-elements-skrn" ), 
 		'query_var' => true, 
 		'rewrite' => array('slug' => 'media-director'),
 		)
 	 );
	 
  	register_taxonomy(
  		'video-cast', 'video_skrn', 
  		array('hierarchical' => true, 
  		'label' => esc_html__( "Instructori", "progression-elements-skrn" ), 
  		'query_var' => true, 
  		'rewrite' => array('slug' => 'biografie-instructor'),
  		)
  	 );
 	

}

