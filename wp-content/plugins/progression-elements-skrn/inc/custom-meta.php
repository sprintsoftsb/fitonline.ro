<?php

add_action( 'cmb2_admin_init', 'progression_studios_page_meta_box' );
function progression_studios_page_meta_box() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'progression_studios_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$progression_studios_cmb = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_page_settings',
		'title'         => esc_html__('Page Settings', 'progression-elements-skrn'),
		'object_types'  => array( 'page' ), // Post type,
	) );
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Sub-title', 'progression-elements-skrn'),
		'id'         => $prefix . 'sub_title',
		'type'       => 'text',
	) );

	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Sidebar Display', 'progression-elements-skrn'),
		'id'         => $prefix . 'page_sidebar',
		'type'       => 'select',
		'options'     => array(
			'hidden-sidebar'   => esc_html__( 'Hide Sidebar', 'progression-elements-skrn' ),
			'right-sidebar'    => esc_html__( 'Right Sidebar', 'progression-elements-skrn' ),
			'left-sidebar'    => esc_html__( 'Left Sidebar', 'progression-elements-skrn' ),
		),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Page Title Background Image', 'progression-elements-skrn'),
		'id'         => $prefix . 'header_image',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Disable Page Title', 'progression-elements-skrn'),
		'id'         => $prefix . 'disable_page_title',
		'type'       => 'checkbox',
	) );
	
}


add_action( 'cmb2_admin_init', 'progression_studios_index_post_meta_box' );
function progression_studios_index_post_meta_box() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'progression_studios_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$progression_studios_cmb = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_post',
		'title'         => esc_html__('Post Settings', 'progression-elements-skrn'),
		'object_types'  => array( 'post' ), // Post type
	) );
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Image Gallery', 'progression-elements-skrn'),
		'desc' => esc_html__('Add-in images to display a gallery.', 'progression-elements-skrn'),
		'id'         => $prefix . 'gallery',
		'type'         => 'file_list',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Video/Audio', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url or embed code', 'progression-elements-skrn'),
		'id'         => $prefix . 'video_post',
		'type'       => 'textarea_code',
		'options' => array( 'disable_codemirror' => true )
	) );

	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Featured Image Link', 'progression-elements-skrn'),
		'id'         => $prefix . 'blog_featured_image_link',
		'type'       => 'select',
		'options'     => array(
			'progression_link_default'   => esc_html__( 'Default link to post', 'progression-elements-skrn' ), // {#} gets replaced by row number
			'progression_link_lightbox'    => esc_html__( 'Link to image in lightbox pop-up', 'progression-elements-skrn' ),
			'progression_link_url'    => esc_html__( 'Link to URL', 'progression-elements-skrn' ),
			'progression_link_url_new_window'    => esc_html__( 'Link to URL (New Window)', 'progression-elements-skrn' ),
			'progression_lightbox_video'    => esc_html__( 'Link to video (Youtube/Vimeo/.MP4)', 'progression-elements-skrn' ),
		),

	) );
	

	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Optional Link', 'progression-elements-skrn'),
		'desc' => esc_html__('Make your post link to another page or video pop-up.', 'progression-elements-skrn'),
		'id'         => $prefix . 'external_link',
		'type'       => 'text',
	) );
	


	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Disable Sidebar on Post', 'progression-elements-skrn'),
		'id'         => $prefix . 'group_552033a9a46bc',
		'type'       => 'checkbox',
	) );
	
}













add_action( 'cmb2_admin_init', 'progression_studios_media_meta_box' );
function progression_studios_media_meta_box() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'progression_studios_';
	
	
	
	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$progression_studios_cmb = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_media_settings',
		'title'         => esc_html__('Video Settings', 'progression-elements-skrn'),
		'object_types'  => array( 'video_skrn' ), // Post type,
		'tabs'      => array(
				'default_tab' => array(
					'label' => esc_html__( 'Default Settings', 'progression-elements-skrn' ),
					'icon'  => 'dashicons-admin-generic', // Dashicon
				),
				'social_tab'  => array(
					'label' => esc_html__( 'Meta Settings', 'progression-elements-skrn' ),
					'icon'  => 'dashicons-admin-tools', // Dashicon
				),
				'slider_tab'  => array(
					'label' => esc_html__( 'Slider Settings', 'progression-elements-skrn' ),
					'icon'  => 'dashicons-slides', // Dashicon
				),
				'season_1_tab'  => array(
					'label' => esc_html__( 'Season 1', 'progression-elements-skrn' ),
					'icon'  => 'dashicons-media-default', // Dashicon
				),
				'season_2_tab'  => array(
					'label' => esc_html__( 'Season 2', 'progression-elements-skrn' ),
					'icon'  => 'dashicons-media-default', // Dashicon
				),
				'season_3_tab'  => array(
					'label' => esc_html__( 'Season 3', 'progression-elements-skrn' ),
					'icon'  => 'dashicons-media-default', // Dashicon
				),
				'season_4_tab'  => array(
					'label' => esc_html__( 'Season 4', 'progression-elements-skrn' ),
					'icon'  => 'dashicons-media-default', // Dashicon
				),
				'season_5_tab'  => array(
					'label' => esc_html__( 'Season 5', 'progression-elements-skrn' ),
					'icon'  => 'dashicons-media-default', // Dashicon
				),
			),
			'tab_style'   => 'default',
	) );
	
	

	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Index Link', 'progression-elements-skrn'),
		'id'         => $prefix . 'blog_featured_image_link',
		'type'       => 'select',
		'options'     => array(
			'progression_link_default'   => esc_html__( 'Default link to post', 'progression-elements-skrn' ), // {#} gets replaced by row number
			'progression_link_url'    => esc_html__( 'Link to URL', 'progression-elements-skrn' ),
			'progression_link_url_new_window'    => esc_html__( 'Link to URL (New Window)', 'progression-elements-skrn' ),
		),
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	

	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Optional Index Link', 'progression-elements-skrn'),
		'desc' => esc_html__('Make your index post link to another page', 'progression-elements-skrn'),
		'id'         => $prefix . 'external_link',
		'type'       => 'text',
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_blog_featured_image_link',
		            'data-conditional-value'  => wp_json_encode( array('progression_link_url', 'progression_link_url_new_window' ) ),
		 ),
	) );
	
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Sidebar Poster', 'progression-elements-skrn'),
		'desc' => esc_html__('Optional custom sidebar poster image. Default display is the Featured Image', 'progression-elements-skrn'),
		'id'         => $prefix . 'poster_image',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Header Background', 'progression-elements-skrn'),
		'id'         => $prefix . 'header_image',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Hosted Video (.mp4)', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url', 'progression-elements-skrn'),
		'id'         => $prefix . 'video_post',
		'type'       => 'text',
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Locally Hosted Video Poster', 'progression-elements-skrn'),
		'id'         => $prefix . 'video_embed_poster',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Youtube Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL after ?v=', 'progression-elements-skrn'),
		'id'         => $prefix . 'youtube_video',
		'type'       => 'text',
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Vimeo Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL', 'progression-elements-skrn'),
		'id'         => $prefix . 'vimeo_video',
		'type'       => 'text',
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Embedded Video/Audio', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url or embed code', 'progression-elements-skrn'),
		'id'         => $prefix . 'video_embed',
		'type'       => 'textarea_code',
		'options' => array( 'disable_codemirror' => true ),
		'tab'  => 'default_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Film Rating (PG13, R)', 'progression-elements-skrn'),
		'id'         => $prefix . 'film_rating',
		'type'       => 'text',
		'tab'  => 'social_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Resolution (SD, HD, 4K)', 'progression-elements-skrn'),
		'id'         => $prefix . 'screen_resolution',
		'type'       => 'text',
		'tab'  => 'social_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Release Date', 'progression-elements-skrn'),
		'id'         => $prefix . 'release_date',
		'type'       => 'text_date',
		'tab'  => 'social_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Duration', 'progression-elements-skrn'),
		'id'         => $prefix . 'media_duration_meta',
		'type'       => 'text',
		'tab'  => 'social_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Search Duration', 'progression-elements-skrn'),
		'id'         => $prefix . 'media_duration',
		'type'       => 'select',
		'options'     => array(
			'short'   => esc_html__( 'Short (< 5 minutes)', 'progression-elements-skrn' ),
			'medium'    => esc_html__( 'Medium (5-10 minutes)', 'progression-elements-skrn' ),
			'long'    => esc_html__( 'Long (> 10 minutes)', 'progression-elements-skrn' ),
		),
		'tab'  => 'social_tab',
		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	
	
	
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Dark Slider Text', 'progression-elements-skrn'),
		'desc'       => esc_html__('Check box for slider text to display dark', 'progression-elements-skrn'),
		'id'         => $prefix . 'slider_dark_version',
		'type'       => 'checkbox',
 		'tab'  => 'slider_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Slider Header Image', 'progression-elements-skrn'),
		'id'         => $prefix . 'slider_header_image',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
 		'tab'  => 'slider_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Button Icon', 'progression-elements-skrn'),
		'desc'       => esc_html__('View list of Icons: https://fontawesome.com/icons?d=gallery&m=free', 'progression-elements-skrn'),
		'id'         => $prefix . 'slider_btn_icon',
		'type'       => 'text',
 		'tab'  => 'slider_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Button Text', 'progression-elements-skrn'),
		'id'         => $prefix . 'slider_btn_text',
		'type'       => 'text',
 		'tab'  => 'slider_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Button Target', 'progression-elements-skrn'),
		'id'         => $prefix . 'slider_btn_target',
		'type'       => 'select',
		'options'     => array(
			'link'    => esc_html__( 'Page Link', 'progression-elements-skrn' ),
			'youtube_video'   => esc_html__( 'Youtue Lightbox', 'progression-elements-skrn' ),
			'vimeo_video'   => esc_html__( 'Vimeo Lightbox', 'progression-elements-skrn' ),
			'mp4_video'   => esc_html__( 'Locally Hosted Video', 'progression-elements-skrn' ),
			'external'    => esc_html__( 'Page Link open in New Window', 'progression-elements-skrn' ),
		),
 		'tab'  => 'slider_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Button Link/Video Lightbox', 'progression-elements-skrn'),
		'desc'       => esc_html__('If linking to Youtube or Vimeo videos, use video ID', 'progression-elements-skrn'),
		'id'         => $prefix . 'slider_btn_link',
		'type'       => 'text',
 		'tab'  => 'slider_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Locally Hosted Video Poster', 'progression-elements-skrn'),
		'id'         => $prefix . 'optional_locally_hosted_mp4_button',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
 		'tab'  => 'slider_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_slider_btn_target',
		            'data-conditional-value'  => 'mp4_video',
		 ),
	) );
	
	
	
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Season 1 Title', 'progression-elements-skrn'),
		'id'         => $prefix . 'season_title',
		'type'       => 'text',
 		'tab'  => 'season_1_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$group_field_id = $progression_studios_cmb->add_field( array(
		'id'          => $prefix . 'display_season',
		'type'        => 'group',
		'tab'        => 'season_1_tab',
		'render_row_cb' => array('CMB2_Tabs', 'tabs_render_group_row_cb'),
		'options'     => array(
			'group_title'   => esc_html__( 'Episode {#}', 'progression-elements-skrn' ), // {#} gets replaced by row number
			'add_button'    => esc_html__( 'Add Another Episode', 'progression-elements-skrn' ),
			'remove_button' => esc_html__( 'Remove Episode', 'progression-elements-skrn' ),
			'sortable'      => true, // beta
			'closed'         => true, // true to have the groups closed by default
		),
		
	) );
	
	

	/**
	 * Group fields works the same, except ids only need
	 * to be unique to the group. Prefix is not needed.
	 *
	 * The parent field's id needs to be passed as the first argument.
	 */
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__( 'Episode Title', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_title',
		'type'       => 'text',
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized value
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'        => esc_html__( 'Episode Description', 'progression-elements-skrn' ),
		'id'          => $prefix .  'description',
		'type'        => 'textarea_small',
		'attributes'  => array(
				'rows'        => 3,
			),
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized valu
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__( 'Episode Thumbnail', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_image',
		'type'       => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__('Release Date', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_release_date',
		'type'       => 'text_date',
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__('Duration', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_media_duration_meta',
		'type'       => 'text',
	) );
	
	
	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__('Video Display', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_display',
		'type'       => 'select',
		'options'     => array(
			'local'   => esc_html__( 'Locally Hosted .mp4', 'progression-elements-skrn' ),
			'youtube'    => esc_html__( 'Youtube', 'progression-elements-skrn' ),
			'vimeo'    => esc_html__( 'Vimeo', 'progression-elements-skrn' ),
			'embed'    => esc_html__( 'Embedded Video/Third party', 'progression-elements-skrn' ),
		),
	) );
	
	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__('Hosted Video (.mp4)', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_mp4',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name' => esc_html__('Hosted Video Poster', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_poster',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__('Youtube Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL after ?v=', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_youtube_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'youtube',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__('Vimeo Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_vimeo_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'vimeo',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id, array(
		'name'       => esc_html__('Embedded Video/Audio', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url or embed code', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_embed',
		'type'       => 'textarea_code',
		'options' => array( 'disable_codemirror' => true ),
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'embed',
		 ),
	) );
	
	
	
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Season 2 Title', 'progression-elements-skrn'),
		'id'         => $prefix . 'season_title_two',
		'type'       => 'text',
 		'tab'  => 'season_2_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$group_field_id_two = $progression_studios_cmb->add_field( array(
		'id'          => $prefix . 'display_season_two',
		'type'        => 'group',
		'tab'        => 'season_2_tab',
		'render_row_cb' => array('CMB2_Tabs', 'tabs_render_group_row_cb'),
		'options'     => array(
			'group_title'   => esc_html__( 'Episode {#}', 'progression-elements-skrn' ), // {#} gets replaced by row number
			'add_button'    => esc_html__( 'Add Another Episode', 'progression-elements-skrn' ),
			'remove_button' => esc_html__( 'Remove Episode', 'progression-elements-skrn' ),
			'sortable'      => true, // beta
			'closed'         => true, // true to have the groups closed by default
		),
		
	) );
	
	/**
	 * Group fields works the same, except ids only need
	 * to be unique to the group. Prefix is not needed.
	 *
	 * The parent field's id needs to be passed as the first argument.
	 */
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__( 'Episode Title', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_title',
		'type'       => 'text',
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized value
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'        => esc_html__( 'Episode Description', 'progression-elements-skrn' ),
		'id'          => $prefix .  'description',
		'type'        => 'textarea_small',
		'attributes'  => array(
				'rows'        => 3,
			),
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized valu
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__( 'Episode Thumbnail', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_image',
		'type'       => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__('Release Date', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_release_date',
		'type'       => 'text_date',
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__('Duration', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_media_duration_meta',
		'type'       => 'text',
	) );
	
	
	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__('Video Display', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_display',
		'type'       => 'select',
		'options'     => array(
			'local'   => esc_html__( 'Locally Hosted .mp4', 'progression-elements-skrn' ),
			'youtube'    => esc_html__( 'Youtube', 'progression-elements-skrn' ),
			'vimeo'    => esc_html__( 'Vimeo', 'progression-elements-skrn' ),
			'embed'    => esc_html__( 'Embedded Video/Third party', 'progression-elements-skrn' ),
		),
	) );
	
	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__('Hosted Video (.mp4)', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_mp4',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name' => esc_html__('Hosted Video Poster', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_poster',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__('Youtube Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL after ?v=', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_youtube_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'youtube',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__('Vimeo Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_vimeo_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'vimeo',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_two, array(
		'name'       => esc_html__('Embedded Video/Audio', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url or embed code', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_embed',
		'type'       => 'textarea_code',
		'options' => array( 'disable_codemirror' => true ),
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'embed',
		 ),
	) );
	
	
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Season 3 Title', 'progression-elements-skrn'),
		'id'         => $prefix . 'season_title_three',
		'type'       => 'text',
 		'tab'  => 'season_3_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$group_field_id_three = $progression_studios_cmb->add_field( array(
		'id'          => $prefix . 'display_season_three',
		'type'        => 'group',
		'tab'        => 'season_3_tab',
		'render_row_cb' => array('CMB2_Tabs', 'tabs_render_group_row_cb'),
		'options'     => array(
			'group_title'   => esc_html__( 'Episode {#}', 'progression-elements-skrn' ), // {#} gets replaced by row number
			'add_button'    => esc_html__( 'Add Another Episode', 'progression-elements-skrn' ),
			'remove_button' => esc_html__( 'Remove Episode', 'progression-elements-skrn' ),
			'sortable'      => true, // beta
			'closed'         => true, // true to have the groups closed by default
		),
		
	) );
	
	/**
	 * Group fields works the same, except ids only need
	 * to be unique to the group. Prefix is not needed.
	 *
	 * The parent field's id needs to be passed as the first argument.
	 */
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__( 'Episode Title', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_title',
		'type'       => 'text',
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized value
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'        => esc_html__( 'Episode Description', 'progression-elements-skrn' ),
		'id'          => $prefix .  'description',
		'type'        => 'textarea_small',
		'attributes'  => array(
				'rows'        => 3,
			),
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized valu
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__( 'Episode Thumbnail', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_image',
		'type'       => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Release Date', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_release_date',
		'type'       => 'text_date',
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Duration', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_media_duration_meta',
		'type'       => 'text',
	) );
	
	
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Video Display', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_display',
		'type'       => 'select',
		'options'     => array(
			'local'   => esc_html__( 'Locally Hosted .mp4', 'progression-elements-skrn' ),
			'youtube'    => esc_html__( 'Youtube', 'progression-elements-skrn' ),
			'vimeo'    => esc_html__( 'Vimeo', 'progression-elements-skrn' ),
			'embed'    => esc_html__( 'Embedded Video/Third party', 'progression-elements-skrn' ),
		),
	) );
	
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Hosted Video (.mp4)', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_mp4',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name' => esc_html__('Hosted Video Poster', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_poster',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Youtube Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL after ?v=', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_youtube_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'youtube',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Vimeo Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_vimeo_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'vimeo',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Embedded Video/Audio', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url or embed code', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_embed',
		'type'       => 'textarea_code',
		'options' => array( 'disable_codemirror' => true ),
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'embed',
		 ),
	) );
	
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Season 4 Title', 'progression-elements-skrn'),
		'id'         => $prefix . 'season_title_four',
		'type'       => 'text',
 		'tab'  => 'season_4_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$group_field_id_three = $progression_studios_cmb->add_field( array(
		'id'          => $prefix . 'display_season_four',
		'type'        => 'group',
		'tab'        => 'season_4_tab',
		'render_row_cb' => array('CMB2_Tabs', 'tabs_render_group_row_cb'),
		'options'     => array(
			'group_title'   => esc_html__( 'Episode {#}', 'progression-elements-skrn' ), // {#} gets replaced by row number
			'add_button'    => esc_html__( 'Add Another Episode', 'progression-elements-skrn' ),
			'remove_button' => esc_html__( 'Remove Episode', 'progression-elements-skrn' ),
			'sortable'      => true, // beta
			'closed'         => true, // true to have the groups closed by default
		),
		
	) );
	
	/**
	 * Group fields works the same, except ids only need
	 * to be unique to the group. Prefix is not needed.
	 *
	 * The parent field's id needs to be passed as the first argument.
	 */
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__( 'Episode Title', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_title',
		'type'       => 'text',
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized value
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'        => esc_html__( 'Episode Description', 'progression-elements-skrn' ),
		'id'          => $prefix .  'description',
		'type'        => 'textarea_small',
		'attributes'  => array(
				'rows'        => 3,
			),
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized valu
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__( 'Episode Thumbnail', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_image',
		'type'       => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Release Date', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_release_date',
		'type'       => 'text_date',
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Duration', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_media_duration_meta',
		'type'       => 'text',
	) );
	
	
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Video Display', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_display',
		'type'       => 'select',
		'options'     => array(
			'local'   => esc_html__( 'Locally Hosted .mp4', 'progression-elements-skrn' ),
			'youtube'    => esc_html__( 'Youtube', 'progression-elements-skrn' ),
			'vimeo'    => esc_html__( 'Vimeo', 'progression-elements-skrn' ),
			'embed'    => esc_html__( 'Embedded Video/Third party', 'progression-elements-skrn' ),
		),
	) );
	
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Hosted Video (.mp4)', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_mp4',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name' => esc_html__('Hosted Video Poster', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_poster',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Youtube Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL after ?v=', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_youtube_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'youtube',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Vimeo Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_vimeo_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'vimeo',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Embedded Video/Audio', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url or embed code', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_embed',
		'type'       => 'textarea_code',
		'options' => array( 'disable_codemirror' => true ),
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'embed',
		 ),
	) );
	
	
	
	$progression_studios_cmb->add_field( array(
		'name'       => esc_html__('Season 5 Title', 'progression-elements-skrn'),
		'id'         => $prefix . 'season_title_five',
		'type'       => 'text',
 		'tab'  => 'season_5_tab',
 		'render_row_cb' => array( 'CMB2_Tabs', 'tabs_render_row_cb' ),
	) );
	
	
	$group_field_id_three = $progression_studios_cmb->add_field( array(
		'id'          => $prefix . 'display_season_five',
		'type'        => 'group',
		'tab'        => 'season_5_tab',
		'render_row_cb' => array('CMB2_Tabs', 'tabs_render_group_row_cb'),
		'options'     => array(
			'group_title'   => esc_html__( 'Episode {#}', 'progression-elements-skrn' ), // {#} gets replaced by row number
			'add_button'    => esc_html__( 'Add Another Episode', 'progression-elements-skrn' ),
			'remove_button' => esc_html__( 'Remove Episode', 'progression-elements-skrn' ),
			'sortable'      => true, // beta
			'closed'         => true, // true to have the groups closed by default
		),
		
	) );
	
	/**
	 * Group fields works the same, except ids only need
	 * to be unique to the group. Prefix is not needed.
	 *
	 * The parent field's id needs to be passed as the first argument.
	 */
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__( 'Episode Title', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_title',
		'type'       => 'text',
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized value
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'        => esc_html__( 'Episode Description', 'progression-elements-skrn' ),
		'id'          => $prefix .  'description',
		'type'        => 'textarea_small',
		'attributes'  => array(
				'rows'        => 3,
			),
		'sanitization_cb' => 'progression_studios_html_allow', // function should return a sanitized valu
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__( 'Episode Thumbnail', 'progression-elements-skrn' ),
		'id'         => $prefix .  'episode_image',
		'type'       => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Release Date', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_release_date',
		'type'       => 'text_date',
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Duration', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_media_duration_meta',
		'type'       => 'text',
	) );
	
	
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Video Display', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_display',
		'type'       => 'select',
		'options'     => array(
			'local'   => esc_html__( 'Locally Hosted .mp4', 'progression-elements-skrn' ),
			'youtube'    => esc_html__( 'Youtube', 'progression-elements-skrn' ),
			'vimeo'    => esc_html__( 'Vimeo', 'progression-elements-skrn' ),
			'embed'    => esc_html__( 'Embedded Video/Third party', 'progression-elements-skrn' ),
		),
	) );
	
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Hosted Video (.mp4)', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_mp4',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name' => esc_html__('Hosted Video Poster', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_poster',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'local',
		 ),
	) );
	

	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Youtube Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL after ?v=', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_youtube_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'youtube',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Vimeo Video (ID)', 'progression-elements-skrn'),
		'desc'       => esc_html__('You can find the id at the end of the URL', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_vimeo_video',
		'type'       => 'text',
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'vimeo',
		 ),
	) );
	
	$progression_studios_cmb->add_group_field( $group_field_id_three, array(
		'name'       => esc_html__('Embedded Video/Audio', 'progression-elements-skrn'),
		'desc'       => esc_html__('Paste in your video url or embed code', 'progression-elements-skrn'),
		'id'         => $prefix . 'episode_video_embed',
		'type'       => 'textarea_code',
		'options' => array( 'disable_codemirror' => true ),
		'attributes'    => array(
		            'data-conditional-id'     => 'progression_studios_episode_video_display',
		            'data-conditional-value'  => 'embed',
		 ),
	) );

	
}






add_action( 'cmb2_admin_init', 'progression_studios_cast_taxonomy_meta_box' );
function progression_studios_cast_taxonomy_meta_box() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'progression_studios_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$progression_studios_cmb = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_media_cast_settings',
		'title'         => esc_html__('Post Meta', 'progression-elements-skrn'),
		'object_types'     => array( 'term' ),
		'taxonomies'       => array( 'video-cast', 'video-director', 'video-type', 'video-genres', 'video-category' ),
	) );
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Image', 'progression-elements-skrn'),
		'id'         => $prefix . 'cast_Photo',
		'type'         => 'file',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
	

	

}





add_action( 'cmb2_admin_init', 'progression_studios_notifications_taxonomy_meta_box' );
function progression_studios_notifications_taxonomy_meta_box() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'progression_studios_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$progression_studios_cmb = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_media_notifications_settings',
		'title'         => esc_html__('Settings', 'progression-elements-skrn'),		
		'object_types'  => array( 'notifications_skrn' ), // Post type,
		
		
	) );
	
	$progression_studios_cmb->add_field( array(
		'name' => esc_html__('Link to Post', 'progression-elements-skrn'),
		'id'         => $prefix . 'notification_link',
		'type'         => 'text',
	) );
	

	

}









