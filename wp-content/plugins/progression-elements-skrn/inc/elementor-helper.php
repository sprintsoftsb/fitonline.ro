<?php
namespace Elementor;

function progression_skrn_elements_elementor_init(){
    Plugin::instance()->elements_manager->add_category(
        'progression-elements-skrn-cat',
        [
            'title'  => 'SKRN Addons',
            'icon' => 'font'
        ],
        1
    );
}
add_action('elementor/init','Elementor\progression_skrn_elements_elementor_init');



//Query Categories List
function skrn_elements_post_type_categories(){
	//https://developer.wordpress.org/reference/functions/get_terms/
	$terms = get_terms( array( 
		'taxonomy' => 'video-category',
		'hide_empty' => true,
	));
	
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
	foreach ( $terms as $term ) {
		$options[ $term->slug ] = $term->name;
	}
	return $options;
	}
	
	
}


function skrn_elements_post_type_types(){
	//https://developer.wordpress.org/reference/functions/get_terms/
	$terms = get_terms( array( 
		'taxonomy' => 'video-type',
		'hide_empty' => true,
	));
	
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
	foreach ( $terms as $term ) {
		$options[ $term->slug ] = $term->name;
	}
	return $options;
	}
	
	
}


function skrn_elements_post_type_genres(){
	//https://developer.wordpress.org/reference/functions/get_terms/
	$terms = get_terms( array( 
		'taxonomy' => 'video-genres',
		'hide_empty' => true,
	));
	
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
	foreach ( $terms as $term ) {
		$options[ $term->slug ] = $term->name;
	}
	return $options;
	}
	
	
}

