<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.


class Widget_ProgressionElementsVideoCarousel extends Widget_Base {

	
	public function get_name() {
		return 'progression-video-post-carousel';
	}

	public function get_title() {
		return esc_html__( 'Carousel - Skrn', 'progression-elements-skrn' );
	}

	public function get_icon() {
		return 'eicon-carousel progression-studios-skrn-pe';
	}

   public function get_categories() {
		return [ 'progression-elements-skrn-cat' ];
	}
	
	public function get_script_depends() { 
		return [ 'owl-carousel' ];
	}
	
	
	function Widget_ProgressionElementsVideoCarousel($widget_instance){
		
	}
	
	protected function _register_controls() {

		
  		$this->start_controls_section(
  			'section_title_global_options',
  			[
  				'label' => esc_html__( 'Carousel Settings', 'progression-elements-skrn' )
  			]
  		);
		
		$this->add_control(
			'boosted_post_list_text_title',
			[
				'label' => esc_html__( 'Section Heading', 'progression-elements-progression' ),
				'type' => Controls_Manager::TEXT,
			]
		);
		
		$this->add_control(
			'progression_main_post_count',
			[
				'label' => esc_html__( 'Post Count', 'progression-elements-skrn' ),
				'type' => Controls_Manager::NUMBER,
				'default' => '6',
			]
		);
		
		$this->add_control(
			'progression_elements_image_grid_column_count',
			[
  				'label' => esc_html__( 'Columns Desktop', 'progression-elements-skrn' ),
				'type' => Controls_Manager::NUMBER,				
				'default' => '4',
				'render_type' => 'template'
			]
		);
		
		$this->add_control(
			'progression_elements_image_grid_column_count_tablet',
			[
  				'label' => esc_html__( 'Columns Tablet', 'progression-elements-skrn' ),
				'type' => Controls_Manager::NUMBER,				
				'default' => '2',
				'render_type' => 'template'
			]
		);
		
		$this->add_control(
			'progression_elements_image_grid_column_count_mobile',
			[
  				'label' => esc_html__( 'Columns Mobile', 'progression-elements-skrn' ),
				'type' => Controls_Manager::NUMBER,				
				'default' => '1',
				'render_type' => 'template'
			]
		);
		
		
  		$this->add_control(
  			'progression_elements_image_grid_margin',
  			[
  				'label' => esc_html__( 'Margin', 'progression-elements-skrn' ),
  				'type' => Controls_Manager::NUMBER,
				'default' => '30',
				'render_type' => 'template'
  			]
  		);
		
		
		
		$this->add_control(
			'carousel_autoplay',
			[
				'label' => esc_html__( 'Autoplay', 'progression-elements-progression' ),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
			]
		);
  		$this->add_control(
  			'carousel_autoplay_duration_number',
  			[
  				'label' => esc_html__( 'Autoplay Duration', 'progression-elements-skrn' ),
  				'type' => Controls_Manager::NUMBER,
				'default' => '5000',
				'condition' => [
					'carousel_autoplay' => 'yes',
				],
  			]
  		);
		
		
		$this->add_control(
			'carousle_slide_by',
			[
  				'label' => esc_html__( 'Slide by', 'progression-elements-skrn' ),
				'type' => Controls_Manager::NUMBER,				
				'default' => '1',
				'render_type' => 'template'
			]
		);
		
		
		$this->add_control(
			'carousel_loop',
			[
				'label' => esc_html__( 'Loop Carousel', 'progression-elements-progression' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'return_value' => 'yes',
			]
		);
		
		
		$this->add_control(
			'carousel_nav_on',
			[
				'label' => esc_html__( 'Carousel Arrows', 'progression-elements-progression' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'return_value' => 'yes',
			]
		);
		
		
		$this->add_control(
			'carousel_nav_arrows',
			[
				'label' => esc_html__( 'Always Display Arrows', 'progression-elements-progression' ),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
				'condition' => [
					'carousel_nav_on' => 'yes',
				],
			]
		);
		
		$this->add_control(
			'carousel_dots',
			[
				'label' => esc_html__( 'Carousel Dots', 'progression-elements-progression' ),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
			]
		);
		
		

		
		$this->end_controls_section();
		
		
  		$this->start_controls_section(
  			'section_title_layout_options',
  			[
  				'label' => esc_html__( 'Post Layout', 'progression-elements-skrn' )
  			]
  		);

		

		
		$this->add_control(
			'progression_elements_post_show_rating',
			[
				'label' => esc_html__( 'Display Rating', 'progression-elements-skrn' ),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		

		
		$this->add_control(
			'progression_elements_post_display_excerpt',
			[
				'label' => esc_html__( 'Display Excerpt', 'progression-elements-skrn' ),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
			]
		);
		
		
  		$this->add_control(
  			'progression_elements_image_hover_transparency',
  			[
  				'label' => esc_html__( 'Image Transparency (Hover)', 'progression-elements-skrn' ),
  				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .progression-studios-feaured-video-index:hover a img' => 'opacity:{{SIZE}};',
				],
  			]
  		);
		
		
		$this->add_control(
			'progression_elements_image_hover_effect',
			[
  				'label' => esc_html__( 'Image Hover Effect', 'progression-elements-skrn' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT,				
				'default' => 'progression-studios-blog-image-no-effect',
				'options' => [
					'progression-studios-blog-image-no-effect' => esc_html__( 'No effect', 'progression-elements-skrn' ),
					'progression-studios-blog-image-scale' => esc_html__( 'Zoom', 'progression-elements-skrn' ),
					'progression-studios-blog-image-zoom-grey' => esc_html__( 'Greyscale', 'progression-elements-skrn' ),
					'progression-studios-blog-image-zoom-sepia' => esc_html__( 'Sepia', 'progression-elements-skrn' ),
					'progression-studios-blog-image-zoom-saturate' => esc_html__( 'Saturate', 'progression-elements-skrn' ),
					'progression-studios-blog-image-zoom-shine' => esc_html__( 'Shine', 'progression-elements-skrn' ),
				],
			]
		);
		
		

		
		$this->end_controls_section();
		
		
  		$this->start_controls_section(
  			'section_title_secondary_options',
  			[
  				'label' => esc_html__( 'Post Query', 'progression-elements-skrn' )
  			]
  		);
		
		
		$this->add_control(
			'progression_post_types',
			[
				'label' => esc_html__( 'Narrow by Filtering Types', 'progression-elements-skrn' ),
				'description' => esc_html__( 'Choose a filtering type to display posts', 'progression-elements-skrn' ),
				'label_block' => true,
				'multiple' => true,
				'type' => Controls_Manager::SELECT2,
				'options' => skrn_elements_post_type_types(),
			]
		);

		
		$this->add_control(
			'progression_post_genres',
			[
				'label' => esc_html__( 'Narrow by Genres', 'progression-elements-skrn' ),
				'description' => esc_html__( 'Choose a genre to display posts', 'progression-elements-skrn' ),
				'label_block' => true,
				'multiple' => true,
				'type' => Controls_Manager::SELECT2,
				'options' => skrn_elements_post_type_genres(),
			]
		);
		
		$this->add_control(
			'progression_post_cats',
			[
				'label' => esc_html__( 'Narrow by Category', 'progression-elements-skrn' ),
				'description' => esc_html__( 'Choose a category to display posts', 'progression-elements-skrn' ),
				'label_block' => true,
				'multiple' => true,
				'type' => Controls_Manager::SELECT2,
				'options' => skrn_elements_post_type_categories(),
			]
		);
		
		
		
		
		
		
		
		$this->add_control(
			'progression_elements_post_order_sorting',
			[
				'label' => esc_html__( 'Order By', 'progression-elements-skrn' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'date' => esc_html__( 'Default - Date', 'progression-elements-skrn' ),
					'title' => esc_html__( 'Post Title', 'progression-elements-skrn' ),
					'menu_order' => esc_html__( 'Menu Order', 'progression-elements-skrn' ),
					'modified' => esc_html__( 'Last Modified', 'progression-elements-skrn' ),
					'comment_count' => esc_html__( 'Comment Count', 'progression-elements-viseo' ),
					'rand' => esc_html__( 'Random', 'progression-elements-skrn' ),
				],
			]
		);
		
		
		$this->add_control(
			'progression_elements_post_order',
			[
				'label' => esc_html__( 'Order', 'progression-elements-skrn' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'ASC' => esc_html__( 'Ascending', 'progression-elements-skrn' ),
					'DESC' => esc_html__( 'Descending', 'progression-elements-skrn' ),
				],
			]
		);
		
		$this->add_control(
			'progression_main_offset_count',
			[
				'label' => esc_html__( 'Offset Count', 'progression-elements-skrn' ),
				'type' => Controls_Manager::NUMBER,
				'default' => '0',
				'description' => esc_html__( 'Use this to skip over posts (Example: 3 would skip the first 3 posts.)', 'progression-elements-skrn' ),
			]
		);
	
		
		$this->end_controls_section();
		
		

		$this->start_controls_section(
			'progression_elements_section_main_styles',
			[
				'label' => esc_html__( 'Main Styles', 'progression-elements-skrn' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		

		
  		$this->add_control(
  			'progression_elements_container_border-radius',
  			[
  				'label' => esc_html__( 'Container Border Radius', 'progression-elements-skrn' ),
  				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .progression-studios-video-index' => 'border-radius: {{SIZE}}px;',
					'{{WRAPPER}} .progression-video-index-content' => 'border-bottom-left-radius: {{SIZE}}px;
border-bottom-right-radius: {{SIZE}}px;',
					'{{WRAPPER}} .progression-studios-feaured-video-index, {{WRAPPER}} .progression-studios-feaured-video-index img' => 'border-top-left-radius: {{SIZE}}px;
border-top-right-radius: {{SIZE}}px;',
				],
  			]
  		);
		
		
		$this->add_control(
			'progression_elements_image_background_color',
			[
				'label' => esc_html__( 'Container Border Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .progression-studios-video-index' => 'border-color: {{VALUE}}',
				],
			]
		);
		
		
		$this->add_responsive_control(
			'progression_elements_content_padding',
			[
				'label' => esc_html__( 'Container Padding', 'progression-elements-skrn' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .progression-video-index-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		
		
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'progression_elements_background_overlay',
				'types' => [ 'classic', 'gradient' ],
				'separator' => 'before',
				'selector' => '{{WRAPPER}} .progression-video-index-content',
			]
		);
		
		
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'boosted_elements_heading_text_shadow',
				'selector' => '{{WRAPPER}} .progression-studios-video-index',
			]
		);
		

		
		$this->end_controls_section();
		

		
		$this->start_controls_section(
			'section_styles_heading_styles',
			[
				'label' => esc_html__( 'Section Heading Styles', 'progression-elements-skrn' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'progression_elements_heading_typography',
				'label' => esc_html__( 'Typography', 'progression-elements-skrn' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} h2.progression-studios-skrn-post-list-title',
			]
		);
		
		$this->add_control(
			'boosted_elements_heading_font_color',
			[
				'label' => esc_html__( 'Title Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} h2.progression-studios-skrn-post-list-title' => 'color: {{VALUE}};',
				],
			]
		);
		
		
		$this->add_responsive_control(
			'progression_elements_secting_heading_margin',
			[
				'label' => esc_html__( 'Section Margin', 'progression-elements-skrn' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} h2.progression-studios-skrn-post-list-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		
		$this->add_responsive_control(
			'progression_elements_secting_heading_align',
			[
				'label' => esc_html__( 'Align', 'progression-elements-skrn' ),
				'type' => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'progression-elements-skrn' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'progression-elements-skrn' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'progression-elements-skrn' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'left',
				'selectors' => [
					'{{WRAPPER}} h2.progression-studios-skrn-post-list-title' => 'text-align: {{VALUE}}',
				],
			]
		);

		
		
		$this->end_controls_section();
		
		
		
		$this->start_controls_section(
			'section_styles_title_styles',
			[
				'label' => esc_html__( 'Title Styles', 'progression-elements-skrn' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'progression_elements_title_typography',
				'label' => esc_html__( 'Typography', 'progression-elements-skrn' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} h2.progression-video-title',
			]
		);
		
		$this->add_control(
			'boosted_elements_title_font_color',
			[
				'label' => esc_html__( 'Title Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} h2.progression-video-title a' => 'color: {{VALUE}};',
				],
			]
		);
		
		
		$this->add_control(
			'boosted_elements_title_hover_font_color',
			[
				'label' => esc_html__( 'Title Hover Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} h2.progression-video-title a:hover' => 'color: {{VALUE}};',
				],
			]
		);
		
		
		$this->add_responsive_control(
			'boosted_elements_title_margin_bottom',
			[
				'label' => esc_html__( 'Title Margin Bottom', 'progression-elements-skrn' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -20,
						'max' => 150,
					],
				],
				'selectors' => [
					'{{WRAPPER}} h2.progression-video-title' => 'margin-bottom: {{SIZE}}px;',
				],
			]
		);
		
		
		
		$this->end_controls_section();
		
		
		
		$this->start_controls_section(
			'section_styles_meta_styles',
			[
				'label' => esc_html__( 'Excerpt Styles', 'progression-elements-skrn' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
				
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'progression_elements_escerpt_typography',
				'label' => esc_html__( 'Typography', 'progression-elements-skrn' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .progression-studios-video-index-excerpt',
			]
		);
		
		
		$this->add_control(
			'progression_elements_excerpt_font_color',
			[
				'label' => esc_html__( 'Excerpt Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .progression-studios-video-index-excerpt' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_responsive_control(
			'boosted_elements_excerpt_margin_bottom',
			[
				'label' => esc_html__( 'Excerpt Margin Bottom', 'progression-elements-skrn' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -50,
						'max' => 150,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .progression-studios-video-index-excerpt p' => 'margin-bottom: {{SIZE}}px;',
				],
			]
		);
		
		$this->end_controls_section();
		
		
		$this->start_controls_section(
			'section_styles_load_more_styles',
			[
				'label' => esc_html__( 'Carousel Arrow Styles', 'progression-elements-skrn' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		
		
		$this->add_responsive_control(
			'progression_elements_load_more_margin',
			[
				'label' => esc_html__( 'Margin', 'progression-elements-skrn' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .progression-own-theme .owl-nav button.owl-next, {{WRAPPER}} .progression-own-theme .owl-nav button.owl-prev' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		

		
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'progression_elements_load_moretypography',
				'label' => esc_html__( 'Typography', 'progression-elements-skrn' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .progression-own-theme .owl-nav button.owl-next, {{WRAPPER}} .progression-own-theme .owl-nav button.owl-prev',
			]
		);
		
		
		
		
		$this->start_controls_tabs( 'boosted_elements_button_tabs' );

		$this->start_controls_tab( 'normal', [ 'label' => esc_html__( 'Normal', 'progression-elements-skrn' ) ] );

		$this->add_control(
			'boosted_elements_button_text_color',
			[
				'label' => esc_html__( 'Text Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .progression-own-theme .owl-nav button.owl-next, {{WRAPPER}} .progression-own-theme .owl-nav button.owl-prev' => 'color: {{VALUE}};',
				],
			]
		);

		
		$this->end_controls_tab();

		$this->start_controls_tab( 'boosted_elements_hover', [ 'label' => esc_html__( 'Hover', 'progression-elements-skrn' ) ] );

		$this->add_control(
			'boosted_elements_button_hover_text_color',
			[
				'label' => esc_html__( 'Text Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .progression-own-theme .owl-nav button.owl-next:hover, {{WRAPPER}} .progression-own-theme .owl-nav button.owl-prev:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();
		$this->end_controls_section();
		
		
		
		
		
		$this->start_controls_section(
			'section_styles_post_sorting_styles',
			[
				'label' => esc_html__( 'Dot Navigation Styles', 'progression-elements-skrn' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		
		$this->add_control(
			'progression_elements_carousel_dot_color',
			[
				'label' => esc_html__( 'Dot Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .progression-own-theme .owl-dots .owl-dot span' => 'background: {{VALUE}}',
				],
			]
		);
		
		
		$this->add_control(
			'progression_elements_carousel_dot_selected',
			[
				'label' => esc_html__( 'Dot Selected Color', 'progression-elements-skrn' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .progression-own-theme .owl-dots .owl-dot.active span' => 'background: {{VALUE}}',
				],
			]
		);
		
		
		$this->add_responsive_control(
			'progression_elements_sorting_padding',
			[
				'label' => esc_html__( 'Dot Margins', 'progression-elements-skrn' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .progression-own-theme .owl-dots .owl-dot span' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		

		
		
		$this->end_controls_section();
		
		
		
		
	}
	

	protected function render( ) {
		
	
	$settings = $this->get_settings();

	global $blogloop;
	global $post;
	
	if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {   $paged = get_query_var('page'); } else {  $paged = 1; }
	

	$post_per_page = $settings['progression_main_post_count'];
	$offset = $settings['progression_main_offset_count'];
	$offset_new = $offset + (( $paged - 1 ) * $post_per_page);
	
	
	
	if ( ! empty( $settings['progression_post_cats'] ) ) {
		$formatarray = $settings['progression_post_cats']; // get custom field value
		
		$catarray = $settings['progression_post_cats']; // get custom field value
		if($catarray >= 1 ) { 
			$catids = implode(', ', $catarray); 
		} else {
			$catids = '';
		}
		
		if($formatarray >= 1) { 
			$formatids = implode(', ', $formatarray);
         $formatidsexpand = explode(', ', $formatids);
			$formatoperator = 'IN'; 
		} else {
			$formatidsexpand = '';
			$formatoperator = 'NOT IN'; 
		}
		$operator = 'IN';
 	} else {

	 		$formatidsexpand = '';
			$operator = 'NOT IN';
 	}
	
	
	if ( ! empty( $settings['progression_post_types'] ) ) {
		$formattagarray = $settings['progression_post_types']; // get custom field value
		
		$tagarray = $settings['progression_post_types']; // get custom field value
		if($tagarray >= 1 ) { 
			$cattagidss = implode(', ', $tagarray); 
		} else {
			$cattagidss = '';
		}
		
		if($formattagarray >= 1) { 
			$formatagids = implode(', ', $formattagarray);
         $formatagidsexpand = explode(', ', $formatagids);
			$formatotagperator = 'IN'; 
		} else {
			$formatagidsexpand = '';
			$formatotagperator = 'NOT IN'; 
		}
		$tagoperator = 'IN';
 	} else {

	 		$formatagidsexpand = '';
			$tagoperator = 'NOT IN';
 	}
	
	
	
	if ( ! empty( $settings['progression_post_genres'] ) ) {
		$formatgenrearray = $settings['progression_post_genres']; // get custom field value
		
		$genrearray = $settings['progression_post_genres']; // get custom field value
		if($genrearray >= 1 ) { 
			$catgenreidss = implode(', ', $genrearray); 
		} else {
			$catgenreidss = '';
		}
		
		if($formatgenrearray >= 1) { 
			$formagenreids = implode(', ', $formatgenrearray);
         $formagenreidsexpand = explode(', ', $formagenreids);
			$formatogenreperator = 'IN'; 
		} else {
			$formagenreidsexpand = '';
			$formatogenreperator = 'NOT IN'; 
		}
		$genreoperator = 'IN';
 	} else {

	 		$formagenreidsexpand = '';
			$genreoperator = 'NOT IN';
 	}
	
	
	
 	$args = array(
 	        'post_type'         => 'video_skrn',
			  'orderby'         => $settings['progression_elements_post_order_sorting'],
			  'order'         => $settings['progression_elements_post_order'],
			  'ignore_sticky_posts' => 1,
			  'posts_per_page'     =>  $post_per_page,
			  'paged' => $paged,
			  'offset' => $offset_new,
			  'tax_query' => array(
				  'relation' => 'AND',
		        array(
		            'taxonomy' => 'video-category',
		            'field'    => 'slug',
		            'terms'    => $formatidsexpand,
						'operator' => $operator
		        ),
		        array(
	            'taxonomy' => 'video-type',
		            'field'    => 'slug',
		            'terms'    => $formatagidsexpand,
						'operator' => $tagoperator
		        ),
		        array(
	            'taxonomy' => 'video-genres',
		            'field'    => 'slug',
		            'terms'    => $formagenreidsexpand,
						'operator' => $genreoperator
		        ),
			  ),
 	);

	$blogloop = new \WP_Query( $args );
	?>
	
	
	<div class="progression-studios-elementor-carousel-container <?php if ( ! empty( $settings['carousel_nav_arrows'] ) ) : ?>progression-studios-always-arrows-on<?php endif; ?>">
		
		<?php if ( $settings['boosted_post_list_text_title'] ) : ?>
			<h2 class="progression-studios-skrn-post-list-title"><?php echo esc_attr( $settings['boosted_post_list_text_title'] ); ?></h2>
		<?php endif; ?>
		
		<div id="progression-video-carousel-<?php echo esc_attr($this->get_id()); ?>" class="owl-carousel progression-own-theme">
		<?php while($blogloop->have_posts()): $blogloop->the_post();?>
		<div class="item">
			<?php include(locate_template('template-parts/content-video-elementor.php')); ?>
		</div>
		<?php  endwhile; // end of the loop. ?>
		</div><!-- close #progression-video-carousel-<?php echo esc_attr($this->get_id()); ?> -->
		
	</div><!-- close .progression-studios-elementor-carousel-container -->
	
	<div class="clearfix-pro"></div>

	<?php wp_reset_postdata();?>
	
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		'use strict';
		
		$('#progression-video-carousel-<?php echo esc_attr($this->get_id()); ?>').owlCarousel({
		    margin:<?php echo esc_attr( $settings['progression_elements_image_grid_margin'] ); ?>,
		    items:<?php echo esc_attr( $settings['progression_elements_image_grid_column_count'] ); ?>,
			autoplay:<?php if ( ! empty( $settings['carousel_autoplay'] ) ) : ?>true<?php else: ?>false<?php endif; ?>,
			autoplayTimeout:<?php echo esc_attr( $settings['carousel_autoplay_duration_number'] ); ?>,
			nav: <?php if ( ! empty( $settings['carousel_nav_on'] ) ) : ?>true<?php else: ?>false<?php endif; ?>,
			slideBy:<?php echo esc_attr( $settings['carousle_slide_by'] ); ?>,
		    loop:<?php if ( ! empty( $settings['carousel_loop'] ) ) : ?>true<?php else: ?>false<?php endif; ?>,//match with rewind
			rewind: <?php if ( ! empty( $settings['carousel_loop'] ) ) : ?>true<?php else: ?>false<?php endif; ?>,
			dots: <?php if ( ! empty( $settings['carousel_dots'] ) ) : ?>true<?php else: ?>false<?php endif; ?>,
			autoplayHoverPause:true,
			responsive : {
			    // breakpoint from 0 up
			    0 : {
			        items:<?php echo esc_attr( $settings['progression_elements_image_grid_column_count_mobile'] ); ?>,
			    },
			    // breakpoint from 768 up
			    768 : {
			        items:<?php echo esc_attr( $settings['progression_elements_image_grid_column_count_tablet'] ); ?>,
			    },
			    // breakpoint from 1025 up
			    1025 : {
			        items:<?php echo esc_attr( $settings['progression_elements_image_grid_column_count'] ); ?>,
			    }
			}
		});
		
	});
	</script>
	

	<?php
	
	}

	protected function content_template(){}
}


Plugin::instance()->widgets_manager->register_widget_type( new Widget_ProgressionElementsVideoCarousel() );