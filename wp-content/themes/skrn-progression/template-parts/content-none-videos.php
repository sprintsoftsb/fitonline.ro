<?php
/**
 * @package pro
 */
?>
<section class="no-results-pro not-found-pro">
	
	<h2 class="page-title-pro"><?php esc_html_e( 'No Videos Found', 'skrn-progression' ); ?></h2>

	<div class="page-content-pro">
		<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with different keywords.', 'skrn-progression' ); ?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->