<?php /* Template Name: Add Video */ ?>
<?php
	get_header();

	global $post_type, $post_type_object, $post;
	wp_enqueue_media();
	wp_enqueue_script(
		'media-uploader',
		get_stylesheet_directory_uri() . '/js/media-uploader.js',
		array( 'jquery' )
	);
	$is_authorized = false;
	foreach($current_user->roles as $role) {
		if($role == 'contributor' || $role == 'administrator' || $role == 'admin') {
			$is_authorized = true;
		}
	}
	if(!$is_authorized) {
		wp_redirect(get_home_url());
	}

	if ( 'add-video.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {
		$video_types = get_terms([
			'taxonomy' => 'video-type',
			'hide_empty' => false,
			'parent' => 0,
		]);
		$video_genres = get_terms([
			'taxonomy' => 'video-genres',
			'hide_empty' => false,
			'parent' => 0,
		]);
	
?>
	<div id="progression-studios-sidebar-col-main">

		
		<?php if(!get_post_meta($post->ID, 'progression_studios_disable_page_title', true)  ): ?>
		<div id="page-title-pro">
				<div id="progression-studios-page-title-container">
					<div class="width-container-pro">
					<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
					<?php if(get_post_meta($post->ID, 'progression_studios_sub_title', true)): ?><h4 class="progression-sub-title"><?php echo wp_kses( get_post_meta($post->ID, 'progression_studios_sub_title', true) , true); ?></h4><?php endif; ?>
					</div><!-- close .width-container-pro -->
				</div><!-- close #progression-studios-page-title-container -->
				<div class="clearfix-pro"></div>
		</div><!-- #page-title-pro -->
		<?php endif; ?>
		
		<div class="dashboard-container-pro<?php if(get_post_meta($post->ID, 'progression_studios_page_sidebar', true) == 'left-sidebar' ) : ?> left-sidebar-pro<?php endif; ?>">


			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
			<?php endwhile; ?>
			
			
		<div class="clearfix-pro"></div>
		</div><!-- close .dashboard-container-pro -->
	</div>
<div id="progression-studios-sidebar-col-main" class="add-video-page">
<form action="" method="post" name="add_video_form" id="add_video_form">

	<div class="row">
		<div class="col-lg-12 input-center">
			<!-- TITLU -->
			<label for="title" class="title-label">Title:</label><br>
			<input type="text" id="title" name="title" class="text-input-add-video"><br>
		</div>

		<div class="col-lg-12 input-center">
			<!-- DESCRIERE -->

			<label for="description" class="title-label">Description:</label><br>
			<textarea type="text" id="description" name="description" class="text-input-add-video" style="height: 35px;"></textarea><br>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 input-center">
			<!-- IMG REPREZENTATIVA - THUMBNAIL -->

			<label for="thumbnail" class="title-label">Choose representative image (thumbnail):</label>
			<div class='thumbnail-preview-wrapper'>
				<img id='thumbnail-preview' width='100' height='100' style='max-height: 50px; width: 50px;'>
			</div>
			<input id="upload_thumbnail_button" type="button" class="button upload-button" value="<?php _e( 'Upload thumbnail' ); ?>" />
			<input type='hidden' name='thumbnail_url' id='thumbnail_url' value=''><br>
		
			<!-- IMG POSTER -->

			<label for="poster" class="title-label">Choose poster image:</label>
			<div class='poster-preview-wrapper'>
				<img id='poster-preview' width='100' height='100' style='max-height: 50px; width: 50px;'>
			</div>
			<input id="upload_poster_button" type="button" class="button upload-button" value="<?php _e( 'Upload poster' ); ?>" />
			<input type='hidden' name='poster_url' id='poster_url' value=''><br>

			<!-- TRAILER -->

			<label for="video" class="title-label">Choose trailer(preview video):</label>
			<div class='trailer-preview-wrapper'>
				<video id='trailer-preview' width='100' height='100' style='max-height: 50px; width: 50px;'>
			</div>
			<input id="upload_trailer_button" type="button" class="button upload-button" value="<?php _e( 'Upload trailer' ); ?>" />
			<input type='hidden' name='trailer_url' id='trailer_url' value=''><br>

			<!-- VIDEO -->

			<label for="video" class="title-label">Choose video:</label>
			<div class='video-preview-wrapper'>
				<video id='video-preview' width='100' height='100' style='max-height: 50px; width: 50px;'>
			</div>
			<input id="upload_video_button" type="button" class="button upload-button" value="<?php _e( 'Upload video' ); ?>" />
			<input type='hidden' name='video_url' id='video_url' value=''><br>
		</div>


		<div class="col-lg-12 input-center">
			<!-- DURATA VIDEO -->
			<label for="video_duration" class="title-label">Video duration:</label><br>
			<input type="text" id="video_duration" name="video_duration" class="text-input-add-video"><br>

			<!-- DURATA VIDEO -->
			<label for="search_duration" class="title-label">Search duration:</label><br>
			<select id="search_duration" name="search_duration" class="text-input-add-video select-input">
				<option value="short">Short (under 5 minutes)</option>
				<option value="medium">Medium (5 - 10 minutes)</option>
				<option value="long">Long (over 10 minutes)</option>
			</select><br>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 input-center">
			<label for="equipment" class="title-label">Equipment needed:</label><br>
			<input type="text" id="video_equipment" name="video_equipment" class="text-input-equipment"><br>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6 input-center">
		<!-- TIPURI VIDEO -->
			<label for="video_types" class="title-label">Video Types:</label><br>

			<div class="video-tipuri">
				<?php foreach($video_types as $type): ?>

					<input type="checkbox" id="type-<?php echo $type->slug; ?>" name="type-<?php echo $type->slug; ?>" value="<?php echo $type->slug; ?>">
					<label for="type-<?php echo $type->slug; ?>"><?php echo $type->name; ?></label><br>

				<?php endforeach; ?>
			</div>
		</div>

		<div class="col-lg-6 input-center">
			<!-- GENURI DE VIDEO -->
			<label for="video_genres" class="title-label">Video Genres:</label><br>

			<div class="video-genuri">
				<?php foreach($video_genres as $genre): ?>

					<input type="checkbox" id="genre-<?php echo $genre->slug; ?>" name="genre-<?php echo $genre->slug; ?>" value="<?php echo $genre->slug; ?>">
					<label for="genre-<?php echo $genre->slug; ?>"><?php echo $genre->name; ?></label><br>

					<?php
						$video_types_children = get_terms([
							'taxonomy' => 'video-genres',
							'hide_empty' => false,
							'parent' => $genre->term_id
						]);
					?>

					<?php foreach($video_types_children as $genre_child): ?>
						<div class="genre-child-div">
							<input type="checkbox" id="genre-<?php echo $genre_child->slug; ?>" name="genre-<?php echo $genre_child->slug; ?>" value="<?php echo $genre_child->slug; ?>">
							<label for="genre-<?php echo $genre_child->slug; ?>"><?php echo $genre_child->name; ?></label><br>
						</div>
					<?php endforeach; ?>

				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<!-- <input type="checkbox" id="gen1" name="gen1" value="gen1">
	<label for="gen1">Animation</label><br>
	<input type="checkbox" id="gen2" name="gen2" value="gen2">
	<label for="gen2">Action</label><br>
	<input type="checkbox" id="gen3" name="gen3" value="gen3">
	<label for="tip3">Adventures</label><br> -->

	<!-- NUME INSTRUCTORI -->
	<!-- <label for="nume-instructori">Nume Instructori:</label><br>
	<input type="checkbox" id="nume1" name="nume1" value="nume1">
	<label for="nume1">Aurica</label><br>
	<input type="checkbox" id="nume2" name="nume2" value="nume2">
	<label for="nume2">Ionel</label><br>
	<input type="checkbox" id="nume3" name="nume3" value="nume3">
	<label for="nume3">Marius</label><br> -->

	<div class="row">
		<div class="col-lg-12 input-center">
			<!-- PRET POST -->
			<label for="plan_amount" class="title-label">Video Price:</label><br>
			<input type="text" id="plan_amount" name="plan_amount" class="text-input-add-video"><br>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 input-center">
			<input type="submit" name="add_video_btn" id="add_video_btn" class="add-video-buton" value="ADD VIDEO">  
		</div>
	</div>
	</form>
</div>

	<?php };
	 get_footer(); ?>