<?php
/**
 * The template for displaying Archive pages.
 * Template Name: Backend Live Stream
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package progression
 */

	$is_authorized = false;
	foreach($current_user->roles as $role) {
		if($role == 'contributor' || $role == 'administrator' || $role == 'admin') {
			$is_authorized = true;
		}
	}
	if(!$is_authorized) {
		wp_redirect(get_home_url());
	}

	global $post_type, $post_type_object, $post;
	wp_enqueue_media();
	wp_enqueue_script(
		'media-uploader',
		get_stylesheet_directory_uri() . '/js/media-uploader.js',
		array( 'jquery' )
	);

	get_header();
?>
<?php 
	$userdata = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); 
?>
	
	<div id="content-sidebar-pro">		
		<div id="content-sidebar-info">
			<div id="avatar-sidebar-large-profile" style="background-image:url('<?php if($userdata->avatar): ?><?php echo esc_url($userdata->avatar);  ?><?php else: ?><?php echo get_avatar_url( $userdata->user_email, 200 ); ?><?php endif; ?>')"></div>

			<div id="profile-sidebar-name">
				<h5><?php if($userdata->first_name): ?>
							<?php echo esc_attr($userdata->first_name); ?> <?php echo esc_attr($userdata->last_name); ?>
						<?php else: ?>
							<?php echo esc_attr($userdata->display_name); ?>
						<?php endif; ?></h5>
				<?php if($userdata->country): ?><h6><?php echo esc_attr($userdata->country); ?></h6><?php endif; ?></h5>
					
					<?php if(function_exists('arm_check_for_wp_rename')  ): ?>
					<?php
					$socialProfileFields = $arm_member_forms->arm_social_profile_field_types();
		         if ($arm_social_feature->isSocialFeature) {
		             if (!empty($socialProfileFields) ) {
						?>	 
							<ul class="profile-social-media-sidebar-icons">
					 
						<?php	 
		            foreach ($socialProfileFields as $spfKey => $spfLabel) {
		                $spfMetaKey = 'arm_social_field_'.$spfKey;
		                $spfMetaValue = get_user_meta($userdata->ID, $spfMetaKey, true);
							 $skey_field = get_user_meta($userdata->ID,$spfMetaKey,true);
						?>
				
				  			 <?php if(!empty($spfMetaValue)): ?>
				  				 <li><a target="_blank" href="<?php echo esc_attr($skey_field);?>"><i class="fab fa-<?php if($spfKey == "googleplush"): ?>google-plus<?php else: ?><?php echo esc_attr($spfKey);?><?php endif; ?>"></i></a></li>
				  				<?php endif; ?>
					<?php } ?>
		 					</ul>
						 <?php
		           }
					}
					?>
					<?php endif; ?>
					
			</div>
			<div id="profile-sidebar-gradient"></div>
			<?php if(get_query_var('author_name')) : $curauth = get_user_by('slug', get_query_var('author_name')); else : $curauth = get_userdata(get_query_var('author')); endif;
				if( $curauth->ID == $user_ID) : ?>
			<?php if(function_exists('arm_check_for_wp_rename')  ): 
				global $arm_global_settings;
				$global_settings = $arm_global_settings->global_settings;
			?>
			<a href="<?php echo esc_url( $arm_global_settings->arm_get_permalink('', $global_settings['edit_profile_page_id']) ); ?>" class="edit-profile-sidebar"><i class="fas fa-pencil-alt"></i></a>
			<?php endif; ?>
			<?php endif; ?>
		</div>
		
		
		<?php

            $fav_videos_loop = new WP_Query( apply_filters( 'widprogression_get_favorite_user_videos', array(
                'posts_per_page'      => 99999,
                'post_status'         => 'publish',
                'ignore_sticky_posts' => true,
                'favorite_videos'    => $userdata->ID,
                'post_type'           => 'video_skrn'
            ) ) );
                
            $wishlist_videos_loop = new WP_Query( apply_filters( 'widprogression_wishlist_videos_user_videos', array(
                'posts_per_page'      => 99999,
                'post_status'         => 'publish',
                'ignore_sticky_posts' => true,
                'wishlist_videos'    => $userdata->ID,
                'post_type'           => 'video_skrn'
            ) ) );
		
		?>

		<?php
		$reiew_count_args = array(
		    'user_id' => $userdata->ID, // comments by this user only
		    'status' => 'approve',
		    'post_status' => 'publish',
		    'post_type' => 'video_skrn'
		);
		
		?>
		
		<?php if (get_theme_mod( 'progression_studios_profile_page_user_stats', 'true') == 'true') : ?>
		<div class="content-sidebar-section">
			<h3 class="content-sidebar-sub-header"><?php esc_html_e( 'User Stats', 'skrn-progression' ); ?></h3>
			<ul id="profile-watched-stats">
				<li><span><?php echo esc_attr($fav_videos_loop->post_count); ?></span> <?php esc_html_e( 'Favorites', 'skrn-progression' ); ?></li>
				<li><span><?php echo esc_attr($wishlist_videos_loop->post_count); ?></span> <?php esc_html_e( 'Watchlist', 'skrn-progression' ); ?></li>
				<li><span><?php echo skrn_custom_count_post_by_author($reiew_count_args); ?></span> <?php esc_html_e( 'Ratings', 'skrn-progression' ); ?></li>
			</ul>
		</div><!-- close .content-sidebar-section -->
		<?php endif; ?>
		
		<?php if(function_exists('arm_check_for_wp_rename')  ): 
		$date_format = $arm_global_settings->arm_get_wp_date_format();
		?>
			<?php if (get_theme_mod( 'progression_studios_profile_page_member_since', 'true') == 'true') : ?>
                <div class="content-sidebar-section">
                    <h3 class="content-sidebar-sub-header"><?php esc_html_e( 'Member Since', 'skrn-progression' ); ?></h3>
                    <div class="content-sidebar-simple-text">
                        <?php echo date_i18n($date_format, strtotime($userdata->user_registered));?>
                    </div>
                </div><!-- close .content-sidebar-section -->
			<a href="/add-video/" class="editeaza-instructor-button">ADD VIDEO</a>
			<?php endif; ?>
			<?php if (get_theme_mod( 'progression_studios_profile_page_biography', 'true') == 'true') : ?>
                <?php if($userdata->description): ?>
                    <div class="content-sidebar-section">
                        <h3 class="content-sidebar-sub-header"><?php esc_html_e( 'Biography', 'skrn-progression' ); ?></h3>
                        <div class="content-sidebar-simple-text">
                            <?php echo esc_attr($userdata->description); ?>
                        </div>
                    </div><!-- close .content-sidebar-section -->
                <?php endif; ?>
			<?php endif; ?>
		<?php endif; ?><!-- end ARMember Actionvation Check -->
	</div><!-- close #content-sidebar-pro -->
	
	
	<div id="col-main-with-sidebar">
		<div class="dashboard-container">
			<div id="skrn-progression-watchlist-div-container">
				<ul class="dashboard-sub-menu">
					<?php if (get_theme_mod( 'progression_studios_profile_page_favorites', 'true') == 'true') : ?><li><a href="/instructor-edit/"><?php esc_html_e( 'My videos', 'skrn-progression' ); ?></a></li><?php endif; ?>
					<?php if (get_theme_mod( 'progression_studios_profile_page_favorites', 'true') == 'true') : ?><li><a href="/editeaza-biografie/"><?php esc_html_e( 'Edit biography', 'skrn-progression' ); ?></a></li><?php endif; ?>
					<li class="current"><a href="/live-stream-instructor/"><?php esc_html_e( 'Live stream coach', 'skrn-progression' ); ?></a></li>
					
				</ul>
                <?php if (!$has_channel): ?>
                    <div class="container-fluid">
                        <form action="" method="post" name="add_channel_form" class="instructor-livestream" id="add_channel_form">
                            <div class="row">
                                <div class="col-lg-12 input-center">
                                    <!-- LINK -->
                                    
                                    <label for="live_link" class="title-label">Youtube live stream link:</label><br>
									<input type="text" id="live_link" name="live_link" class="text-input-add-video"
										value="<?php echo count(get_user_meta(get_current_user_id(), 'live_link')) > 0 ? get_user_meta(get_current_user_id(), 'live_link')[0] : ''; ?>"><br>
                                </div>
                                <!-- <div class="col-lg-12 input-center"> -->
                                    <!-- DESCRIERE -->

                                    <!-- <label for="description" class="title-label">Descriere:</label><br>
                                    <textarea type="text" id="description" name="description" class="text-input-add-video" style="height: 35px;"></textarea><br>
                                </div> -->

                                <div class="col-lg-6 input-center">
                                    <input type="submit" name="add_channel_btn" id="add_channel_btn" class="add-video-buton" value="ADD LIVE">
								</div>
								<?php if (count(get_user_meta(get_current_user_id(), 'live_link')) > 0) : ?>
									<div class="col-lg-6 input-center">
										<input type="submit" name="delete_channel_btn" id="delete_channel_btn" class="add-video-buton delete_btn" value="DELETE LIVE">
									</div>
								<?php endif; ?>
                            </div>
                        </form>
                    </div>
                <?php endif; ?>

			</div><!-- close #skrn-progression-watchlist-div-container -->
		</div><!-- close .dashboard-container -->
	</div>
	

<?php get_footer(); ?>