<?php
/**
 * The template for displaying archive pages.
 * Template Name: Biografie Instructor Page
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package pro
 */
	global $wp_query;
    $instructor = $wp_query->query_vars['instructor'];
    $term = get_term_by('slug', $instructor , 'video-cast');
    $result = 
    $wpdb->get_row( 
       $wpdb->prepare(
         "SELECT * FROM $wpdb->users WHERE `display_name` = %s", $term->name
       ) 
     );
    $instructor_id = $result->ID;
    get_header(); 
?>
	
	<div id="progression-studios-sidebar-col-main">

	<div id="page-title-pro">

		<div id="progression-studios-page-title-container">
			<div class="width-container-pro">
				<?php
				$term = get_term_by( 'slug', $instructor , 'video-cast' );
				$instructor_video = get_term_meta($term->term_id, 'video_instructor')[0];
				$instructor_descriere = get_term_meta($term->term_id, 'descriere_instructor')[0];
				$tax_term_breadcrumb_taxonomy_slug = $term->taxonomy;
				$term_photo = get_term_meta( $term->term_id, 'progression_studios_cast_Photo', true);
				?>
				<h1 class="page-title"><?php if ( !empty( $term_photo ) ) : echo '<div id="skrn-video-cast-photo-taxonomy" style="background-image:url(' . $term_photo . ')"></div>'; endif; ?><?php echo esc_attr('' . $term->name . '');?></h1>
				<?php the_archive_description( '<h4 class="progression-sub-title">', '</h4>' ); ?>
			</div><!-- #progression-studios-page-title-container -->
				<?php if (get_user_meta($instructor_id, 'live_link', true)): ?>
					<a class="btn_live" href="/live-stream/?instructor=<?php echo $instructor; ?>"><span class="live-button blink">LIVE ACUM!</span></a>
				<?php endif; ?>
			<div class="clearfix-pro"></div>
		</div><!-- close .width-container-pro -->
	
	</div><!-- #page-title-pro -->
	
	
		<div class="row descriere-video">
			<div class="col-12 col-lg-6">
				<video controls="" name="media" style="width: 800px;"><source src="<?php echo $instructor_video ?>" type="video/mp4"></video>
			</div>
			<div class="col-12 col-lg-6">
				<h4 class="descriere-instructor-title">Descriere instructor</h4>
				<p class="descriere-instructor-text"><?php echo $instructor_descriere ?></p>
			</div>
		</div>

		<div class="dashboard-container-pro">

			<div id="progression-studios-video-index-list-spacing">
				<?php if ( have_posts() ) : ?>
				<?php
					$args = array(
					'post_type' => 'video_skrn',
					'tax_query' => array(
						array(
						'taxonomy' => 'video-cast',
						'field' => 'term_id',
						'terms' => $term->term_id,
						)
					)
					);
					$query = new WP_Query( $args );
					$posts = $query->posts;
					// echo '<pre>';
					// print_r($query->posts);
					// echo '</pre>';
					?>
				
					<div class="progression-masonry-margins" style="margin-top:-<?php echo esc_attr(get_theme_mod('progression_studios_blog_index_gap', '15')); ?>px; margin-left:-<?php echo esc_attr(get_theme_mod('progression_studios_blog_index_gap', '15')); ?>px; margin-right:-<?php echo esc_attr(get_theme_mod('progression_studios_blog_index_gap', '15')); ?>px;">
						<div class="progression-blog-index-masonry">
							<div class="row">
								<div class="col-12">
									<h2 class="biografie-page-last-added-video">Last videos added</h2>
								</div>
							<?php foreach ($posts as $post): ?>
								<div class="progression-masonry-item col-12 col-lg-3">
									<div class="progression-masonry-padding-blog" style="padding:<?php echo esc_attr(get_theme_mod('progression_studios_blog_index_gap', '15')); ?>px;">
										<div class="progression-studios-isotope-animation">
											
											
											<div id="post-<?php echo $post->ID; ?>" <?php post_class(); ?>>	
												<div class="progression-studios-video-index <?php echo esc_attr( get_theme_mod('progression_studios_blog_transition') ); ?>">

													<?php //if(has_post_thumbnail()): ?>
														<div class="progression-studios-feaured-video-index">
														<a href="<?php echo the_permalink($post->ID); ?>">
														
				
														<img width="500" height="705" src="<?php echo get_post_meta($post->ID, 'progression_studios_poster_image')[0] ?>" class="attachment-progression-studios-video-index size-progression-studios-video-index wp-post-image" alt="" loading="lazy">				</a>				
														<?php //progression_studios_blog_post_title(); ?>
														<?php //the_post_thumbnail('progression-studios-video-index'); ?>
															</a>
														</div><!-- close .progression-studios-feaured-video-index -->
													<?php //endif; ?><!-- close video -->
													
													<div class="progression-video-index-content">
														
														<h2 class="progression-video-title"><?php progression_studios_blog_post_title(); ?><?php the_title(); ?></a></h2>

														<?php if (get_theme_mod( 'progression_studios_video_rating_index_display', 'true') == 'true') : ?>
														<?php if ( skrn_pro_comment_rating_get_average_ratings( $post->ID ) ) : ?>
												      <?php $rating_edit_format = skrn_pro_comment_rating_get_average_ratings( $post->ID );  ?>
														<div
												        class="circle-rating-pro"
												        data-value="<?php if ( $rating_edit_format == '10'  ) : ?>1<?php else: ?>0.<?php echo str_replace(array('.', ','), '' , $rating_edit_format); ?><?php endif; ?>"
												        data-animation-start-value="<?php if ( $rating_edit_format == '10'  ) : ?>1<?php else: ?>0.<?php echo str_replace(array('.', ','), '' , $rating_edit_format); ?><?php endif; ?>"
												        data-size="32"
												        data-thickness="3"
														  
														<?php if ( $rating_edit_format > '6.9'  ) : ?>
													        data-fill="{
													          &quot;color&quot;: &quot;<?php echo esc_attr( get_theme_mod( 'progression_studios_video_rating_color', '#42b740') ); ?>&quot;
													        }"
													        data-empty-fill="<?php echo esc_attr( get_theme_mod( 'progression_studios_video_rating_secondary_color', '#def6de') ); ?>"
													        data-reverse="true"
													      ><span style="color:<?php echo esc_attr( get_theme_mod( 'progression_studios_video_rating_color', '#42b740') ); ?>;">
															<?php else: ?>
														        data-fill="{
														          &quot;color&quot;: &quot;<?php echo esc_attr( get_theme_mod( 'progression_studios_video_rating_negative_color', '#ff4141') ); ?>&quot;
														        }"
														        data-empty-fill="<?php echo esc_attr( get_theme_mod( 'progression_studios_video_rating_negative_secondary_color', '#ffe1e1') ); ?>"
														        data-reverse="true"
														      ><span style="color:<?php echo esc_attr( get_theme_mod( 'progression_studios_video_rating_negative_color', '#ff4141') ); ?>;">
														<?php endif; ?>
														<?php if ( $rating_edit_format == '10'  ) : ?>10<?php else: ?><?php echo number_format((float)$rating_edit_format, 1, '.', '');	?><?php endif; ?></span></div>
														<?php endif; ?>
														
														<?php endif; ?>
														
														
														<?php if (get_theme_mod( 'progression_studios_video_excerpt_display') == 'true') : ?>
															<?php if(has_excerpt() ): ?><div class="clearfix-pro"></div><div class="progression-studios-video-index-excerpt"><?php the_excerpt(); ?></div><?php endif; ?>
														<?php endif; ?>
														
														
														<div class="clearfix-pro"></div>
													</div><!-- close .progression-video-index-content -->
												
												</div><!-- close .progression-studios-video-index -->
											</div><!-- #post-## -->
										

										</div><!-- close .studios-isotope-animation -->
									</div><!-- close .progression-masonry-padding-blog -->
								</div><!-- cl ose .progression-masonry-item -->
								<?php endforeach; ?>
								</div>
						</div><!-- close .progression-blog-index-masonry -->
					</div><!-- close .progression-masonry-margins -->
				
					<div class="clearfix-pro"></div>
					<?php progression_studios_show_pagination_links_pro(); ?>
					<div class="clearfix-pro"></div>
			
				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>
			</div><!-- close #progression-studios-video-index-list-spacing -->
			
			
			<div class="clearfix-pro"></div>
		</div><!-- close .dashboard-container-pro -->
		</div><!-- close #progression-studios-sidebar-col-main -->
	
	<?php get_footer(); ?>