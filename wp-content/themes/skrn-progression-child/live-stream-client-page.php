<?php /* Template Name: Live stream client page */ ?>
<?php
  global $wp_query, $wpdb, $ARMember;

  $instructor = $wp_query->query_vars['instructor'];
  $term = get_term_by( 'slug', $instructor , 'video-cast' );
  $live_link = '';
  $is_paid = false;

  $user_id = get_current_user_id();
  $user = get_user_by('id', $user_id);
  $plans = $wpdb->get_results("SELECT * FROM $ARMember->tbl_arm_subscription_plans");
  foreach($plans as $plan) {
    if($plan->arm_subscription_plan_name == $term->name) {
      $access_plan = $plan->arm_subscription_plan_id;
    }
  };
	$activities = $wpdb->get_results("SELECT * FROM wp_arm_activity WHERE arm_user_id = $user_id");


	if (count($activities) > 0) {
		foreach($activities as $activity) {
			if ($activity->arm_item_id == $access_plan && $activity->arm_action == 'new_subscription') {
				$is_paid = true;
			}
		}
  }

  $result = 
   $wpdb->get_row( 
      $wpdb->prepare(
        "SELECT * FROM $wpdb->users WHERE `display_name` = %s", $term->name
      ) 
    );


  if (count(get_user_meta($result->ID, 'live_link')) > 0) {
    $live_link = str_replace('https://youtu.be/', '', get_user_meta($result->ID, 'live_link')[0]);
  }

  get_header();
?>

<?php if($is_paid): ?>
  <div class="live-stream-cl-page-title"></div>
  <?php if ($live_link == ''): ?>
    <div style="position: relative;top: 130px; margin-left: 405px; font-size: 20px;"><p>Instructorul nu este live momentan!</p></div>
  <?php endif;?>
  <div class="row live-stream-cl-page-wrapper">
    <div class="live-stream-wrapper col-lg-8 col-xs-12 col-sm-12">
      <div id="non-pointer">
        <div id="ytplayer"></div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-12 col-sm-12">
      <?php echo do_shortcode('[rumbletalk-chat hash="aHMwRP2~"]') ?>
    </div>
  </div>
<?php else: ?>
  <div class="col-sm-12 cumpara-plan">
    <p style="font-size: 20px;">Pentru a avea acces la acest plan este nevoie să achiziționați planul instructorului:</p>
    <?php echo '<div class="buy_plan buy_plan_live-stream">' . do_shortcode("[arm_setup id='1' hide_title='false' popup='true' link_type='button' link_title='Cumpara planul instructorului' overlay='0.6' modal_bgcolor='#000000' popup_height='auto' popup_width='800' link_css='' link_hover_css='' hide_plans='1' subscription_plan='" . $access_plan ."']") . '</div>' ?>
    </div>
<?php endif; ?>

<script>

  const tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  const firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  const live_link = "<?php echo $live_link; ?>";
  let player;
  let eventPlayer;
  function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
      width: '500px',
      height: '500px',
      videoId: live_link,
      playerVars: {
        'autoplay': 1,
        'showinfo': 0,
        'autohide': 1,
        'loop': 1,
        'controls': 0,
        'modestbranding': 1,
        'vq': 'hd1080'
      },
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
  }

  function onPlayerReady(event) {
  // event.target.playVideo();
  // player.mute();
  }

  function onPlayerStateChange(event) {
    eventPlayer = event.data;
  }

  jQuery('.live-stream-wrapper').on('click', function() {
      if (eventPlayer == 1) {
        player.pauseVideo();
      } else if (eventPlayer == 3 || eventPlayer == -1 ||eventPlayer == 2) {
        player.playVideo();
      }
  })

</script>

<style>
#non-pointer {
    width: 1625px;
    height: 1220px;
    pointer-events: none;
    text-align: center;
    top: 350px;
    position: relative;
    margin: auto;
}
.live-stream-wrapper {
    position: fixed;
	left: 0;
	z-index: -99;
	position: fixed;
	top: -50%;
	left: -50%;
	width: 200%;
	height: 200%;
}
#non-pointer iframe{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	margin: auto;
	min-width: 50%;
	min-height: 50%;
}
</style>

<?php get_footer(); ?>