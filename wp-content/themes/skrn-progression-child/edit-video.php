<?php /* Template Name: Edit Video */ ?>
<?php
	get_header();

	global $post_type, $post_type_object, $post;

	$is_authorized = false;
	foreach($current_user->roles as $role) {
		if($role == 'contributor' || $role == 'administrator' || $role == 'admin') {
			$is_authorized = true;
		}
	}
	if(!$is_authorized) {
		wp_redirect(get_home_url());
	}
	wp_enqueue_media();
	wp_enqueue_script(
		'media-uploader',
		get_stylesheet_directory_uri() . '/js/media-uploader.js',
		array( 'jquery' )
	);

	if ( 'edit-video.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {
		$video_types = get_terms([
			'taxonomy' => 'video-type',
			'hide_empty' => false,
		]);
		$video_genres = get_terms([
			'taxonomy' => 'video-genres',
			'hide_empty' => false,
		]);
		
		global $wp_query;
		$post_id = $wp_query->query_vars['post_id'];
		$content_post = get_post($post_id);
		$post_meta = get_post_meta($post_id);
		$type_terms = wp_get_object_terms( $post_id,  'video-type' );
		$genre_terms = wp_get_object_terms( $post_id,  'video-genres' );
?>
	<div id="progression-studios-sidebar-col-main">

		
		<?php if(!get_post_meta($post->ID, 'progression_studios_disable_page_title', true)  ): ?>
		<div id="page-title-pro">
				<div id="progression-studios-page-title-container">
					<div class="width-container-pro">
					<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
					<?php if(get_post_meta($post->ID, 'progression_studios_sub_title', true)): ?><h4 class="progression-sub-title"><?php echo wp_kses( get_post_meta($post->ID, 'progression_studios_sub_title', true) , true); ?></h4><?php endif; ?>
					</div><!-- close .width-container-pro -->
				</div><!-- close #progression-studios-page-title-container -->
				<div class="clearfix-pro"></div>
		</div><!-- #page-title-pro -->
		<?php endif; ?>
		
		<div class="dashboard-container-pro<?php if(get_post_meta($post->ID, 'progression_studios_page_sidebar', true) == 'left-sidebar' ) : ?> left-sidebar-pro<?php endif; ?>">


			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
			<?php endwhile; ?>
			
			
		<div class="clearfix-pro"></div>
		</div><!-- close .dashboard-container-pro -->
	</div>
<div id="progression-studios-sidebar-col-main" class="add-video-page">
<form action="" method="post" name="add_video_form" id="add_video_form">
	<input type='hidden' name='post_id' id='post_id' value='<?php echo $post_id; ?>'>
	<div class="row">
		<div class="col-lg-12 input-center">
			<!-- TITLU -->
			<label for="title" class="title-label">Title:</label><br>
			<input type="text" value="<?php echo $content_post->post_title; ?>" id="title" name="title" class="text-input-add-video"><br>
		</div>

		<div class="col-lg-12 input-center">
			<!-- DESCRIERE -->

			<label for="description" class="title-label">Description:</label><br>
			<textarea type="text" id="description" name="description" class="text-input-add-video" style="height: 35px;"><?php echo $content_post->post_content; ?></textarea><br>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 input-center">
			<!-- IMG REPREZENTATIVA - THUMBNAIL -->

			<label for="thumbnail" class="title-label">Choose representative image (thumbnail):</label>
			<div class='thumbnail-preview-wrapper'>
				<img src="<?php echo $post_meta['progression_studios_header_image'][0]; ?>" id='thumbnail-preview' width='100' height='100' style='max-height: 50px; width: 50px;'>
			</div>
			<input id="upload_thumbnail_button" type="button" class="button upload-button" value="<?php _e( 'Upload thumbnail' ); ?>" />
			<input type='hidden' name='thumbnail_url' id='thumbnail_url' value='<?php echo $post_meta['progression_studios_header_image'][0]; ?>'><br>

			<!-- IMG POSTER -->

			<label for="poster" class="title-label">Choose poster image:</label>
			<div class='poster-preview-wrapper'>
				<img src="<?php echo $post_meta['progression_studios_poster_image'][0]; ?>" id='poster-preview' width='100' height='100' style='max-height: 50px; width: 50px;'>
			</div>
			<input id="upload_poster_button" type="button" class="button upload-button" value="<?php _e( 'Upload poster' ); ?>" />
			<input type='hidden' name='poster_url' id='poster_url' value='<?php echo $post_meta['progression_studios_poster_image'][0]; ?>'><br>

			<!-- TRAILER -->

			<label for="video" class="title-label">Choose trailer(preview video):</label>
			<div class='trailer-preview-wrapper'>
				<video src='<?php echo $post_meta['progression_studios_trailer_post'][0]; ?>' id='trailer-preview' width='100' height='100' style='max-height: 50px; width: 50px;'>
			</div>
			<input id="upload_trailer_button" type="button" class="button upload-button" value="<?php _e( 'Upload trailer' ); ?>" />
			<input type='hidden' name='trailer_url' id='trailer_url' value='' value='<?php echo $post_meta['progression_studios_trailer_post'][0]; ?>'><br>

			<!-- VIDEO -->

			<label for="video" class="title-label">Choose video:</label>
			<div class='video-preview-wrapper'>
				<video src='<?php echo $post_meta['progression_studios_video_post'][0]; ?>' id='video-preview' width='100' height='100' style='max-height: 50px; width: 50px;'>
			</div>
			<input id="upload_video_button" type="button" class="button upload-button" value="<?php _e( 'Upload video' ); ?>" />
			<input type='hidden' name='video_url' id='video_url' value='<?php echo $post_meta['progression_studios_video_post'][0]; ?>'><br>
		
		</div>


		<div class="col-lg-12 input-center">
			<!-- DURATA VIDEO -->
			<label for="video_duration" class="title-label">Video duration:</label><br>
			<input type="text" id="video_duration" value="<?php echo $post_meta['progression_studios_media_duration_meta'][0]; ?>" name="video_duration" class="text-input-add-video"><br>

			<!-- DURATA VIDEO -->
			<label for="search_duration" class="title-label">Search duration:</label><br>
			<select id="search_duration" name="search_duration" class="text-input-add-video select-input">
				<option value="short" <?php echo $post_meta['progression_studios_media_duration'][0] == 'short' ? 'selected' : ''; ?>>Short (under 5 minutes)</option>
                <option value="medium" <?php echo $post_meta['progression_studios_media_duration'][0] == 'medium' ? 'selected' : ''; ?>>Medium (5 - 10 minutes)</option>
                <option value="long" <?php echo $post_meta['progression_studios_media_duration'][0] == 'long' ? 'selected' : ''; ?>>Long (over 10 minutes)</option>
			</select><br>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 input-center">
			<label for="equipment" class="title-label">Necesarul de echipament:</label><br>
			<input type="text" id="video_equipment" value="<?php echo $post_meta['progression_studios_equipment_meta'][0]; ?>" name="video_equipment" class="text-input-equipment"><br>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6 input-center">
		<!-- TIPURI VIDEO -->
			<label for="video_types" class="title-label">Tipuri video:</label><br>

			<div class="video-tipuri">
				<?php foreach($video_types as $type): ?>
                            <input type="checkbox" id="type-<?php echo $type->slug; ?>" name="type-<?php echo $type->slug; ?>" value="<?php echo $type->slug; ?>"
                                <?php 
                                    foreach ($type_terms as $term) {
                                        if ($type->term_id == $term->term_id) {
                                            echo 'checked';
                                        }
                                    }
                                ?>
                            >
                            <label for="type-<?php echo $type->slug; ?>"><?php echo $type->name; ?></label><br>
                <?php endforeach; ?>
			</div>
		</div>

		<div class="col-lg-6 input-center">
			<!-- GENURI DE VIDEO -->
			<label for="video_genres" class="title-label">Genuri de video:</label><br>

			<div class="video-genuri">
				<?php foreach($video_genres as $genre): ?>
                    <input type="checkbox" id="genre-<?php echo $genre->slug; ?>" name="genre-<?php echo $genre->slug; ?>" value="<?php echo $genre->slug; ?>"
                        <?php 
                            foreach ($genre_terms as $genre_term) {
                                if ($genre->term_id == $genre_term->term_id) {
                                    echo 'checked';
                                }
                            }
                        ?>
                    >
                    <label for="genre-<?php echo $genre->slug; ?>"><?php echo $genre->name; ?></label><br>
                <?php endforeach; ?>
			</div>
		</div>
	</div>

	<!-- <input type="checkbox" id="gen1" name="gen1" value="gen1">
	<label for="gen1">Animation</label><br>
	<input type="checkbox" id="gen2" name="gen2" value="gen2">
	<label for="gen2">Action</label><br>
	<input type="checkbox" id="gen3" name="gen3" value="gen3">
	<label for="tip3">Adventures</label><br> -->

	<!-- NUME INSTRUCTORI -->
	<!-- <label for="nume-instructori">Nume Instructori:</label><br>
	<input type="checkbox" id="nume1" name="nume1" value="nume1">
	<label for="nume1">Aurica</label><br>
	<input type="checkbox" id="nume2" name="nume2" value="nume2">
	<label for="nume2">Ionel</label><br>
	<input type="checkbox" id="nume3" name="nume3" value="nume3">
	<label for="nume3">Marius</label><br> -->

	<div class="row">
		<div class="col-lg-12 input-center">
			<input type="submit" name="save_video_btn" id="save_video_btn" class="add-video-buton" value="SALVEAZĂ VIDEO">  
		</div>
	</div>
	</form>
</div>

	<?php };
	 get_footer(); ?>