jQuery( document ).ready( function( $ ) {
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    let player;

    function onYouTubePlayerAPIReady() {
        console.log('test');
        
        // create the global player from the specific iframe (#video)
        player = new YT.Player('live-stream-video', {
            events: {
                // call this function when player is ready to use
                'onReady': onPlayerReady
            }
        });

        console.log(player);
    }


onYouTubePlayerAPIReady();
    function onPlayerReady(event) {
        console.log('test1');

        console.log(player);
        // bind events
        var playButton = document.getElementById("play-button");
        playButton.addEventListener("click", function() {
            player.playVideo();
        });
    
        var pauseButton = document.getElementById("pause-button");
        pauseButton.addEventListener("click", function() {
            player.pauseVideo();
        });
    
        var stopButton = document.getElementById("stop-button");
        stopButton.addEventListener("click", function() {
            player.stopVideo();
        });
    
    }
  });