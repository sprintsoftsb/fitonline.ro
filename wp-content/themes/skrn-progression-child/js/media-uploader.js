jQuery( document ).ready( function( $ ) {
  // Uploading files
  var file_frame;
  var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
  // var set_to_post_id = <?php echo $my_saved_attachment_post_id; ?>; // Set this

  jQuery('#upload_thumbnail_button, #upload_poster_button, #upload_video_button, #upload_trailer_button').on('click', function( event ){
    event.preventDefault();

    // Create the media frame.
    file_frame = wp.media.frames.file_frame = wp.media({
      title: 'Select a image to upload',
      button: {
        text: 'Use this image',
      },
      multiple: false	// Set to true to allow multiple files to be selected
    });
    const that = this;
    // When an image is selected, run a callback.
    file_frame.on( 'select', function() {
      // We set multiple to false so only get one image from the uploader
      attachment = file_frame.state().get('selection').first().toJSON();

      if ($(that).attr('id') == 'upload_thumbnail_button') {
        $( '#thumbnail-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
        $( '#thumbnail_url' ).val( attachment.url );
      } else if ($(that).attr('id') == 'upload_poster_button') {
        $( '#poster-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
        $( '#poster_url' ).val( attachment.url );
      } else if ($(that).attr('id') == 'upload_video_button') {
        $( '#video-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
        $( '#video_url' ).val( attachment.url );
        $( '#bio_video_url' ).val( attachment.url );
      } else if ($(that).attr('id') == 'upload_trailer_button') {
        $( '#trailer-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
        $( '#trailer_url' ).val( attachment.url );
      }

      // Restore the main post ID
      wp.media.model.settings.post.id = wp_media_post_id;
    });

      // Finally, open the modal
      file_frame.open();
  });

  // Restore the main ID when the add media button is pressed
  jQuery( 'a.add_media' ).on( 'click', function() {
    wp.media.model.settings.post.id = wp_media_post_id;
  });
});