<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//  
add_action( 'wp_enqueue_scripts', 'skrn_child_progression_studios_enqueue_styles' );
function skrn_child_progression_studios_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}
//
// Your code goes below
//


function get_image_id($image_url) {
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0];
}

function redirect($uri = '', $method = 'auto', $code = NULL)
{
    if ( ! preg_match('#^(\w+:)?//#i', $uri))
    {
        $uri = site_url($uri);
    }

    // IIS environment likely? Use 'refresh' for better compatibility
    if ($method === 'auto' && isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS') !== FALSE)
    {
        $method = 'refresh';
    }
    elseif ($method !== 'refresh' && (empty($code) OR ! is_numeric($code)))
    {
        if (isset($_SERVER['SERVER_PROTOCOL'], $_SERVER['REQUEST_METHOD']) && $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1')
        {
            $code = ($_SERVER['REQUEST_METHOD'] !== 'GET')
                ? 303	// reference: http://en.wikipedia.org/wiki/Post/Redirect/Get
                : 307;
        }
        else
        {
            $code = 302;
        }
    }

    switch ($method)
    {
        case 'refresh':
            header('Refresh:0;url='.$uri);
            break;
        default:
            header('Location: '.$uri, TRUE, $code);
            break;
    }
    exit;
}

add_action( 'init', 'add_video' );
function add_video()
{
    global $ARMember,$wpdb;
    $type_array = array();
    $genres_array = array();
    if($_REQUEST['add_video_btn'] == 'ADAUGA VIDEO') {
        $instructor_name = wp_get_current_user()->display_name;
        
        $result = $wpdb->get_results("SELECT * FROM $ARMember->tbl_arm_subscription_plans");
        foreach($result as $value) {
            if($value->arm_subscription_plan_name == $instructor_name) {
                $plan_id = $value->arm_subscription_plan_id;
            }
        }

        $tax_id = get_term_by('name', $instructor_name, 'video-cast')->term_id;
        $post_arr = array(
            'post_title'    => $_POST['title'],
            'post_content'  => $_POST['description'],
            'post_status'   => 'publish',
            'post_type'     => 'video_skrn',
            'ping_status'   => 'closed',
            'comment_status'    => 'open',
          );
        $post_id = wp_insert_post($post_arr);
        update_post_meta($post_id, 'arm_is_paid_post', 1);
        add_post_meta($post_id, '_edit_lock', round(microtime(true) * 1000) . ':' . get_current_user_id());
        add_post_meta($post_id, '_edit_last', get_current_user_id());
        add_post_meta($post_id, 'progression_studios_blog_featured_image_link', 'progression_link_default');
        add_post_meta($post_id, '_average_ratings', '');
        add_post_meta($post_id, 'progression_studios_slider_btn_target', 'link');
        add_post_meta($post_id, 'progression_studios_media_duration_meta', $_POST['video_duration']);
        add_post_meta($post_id, 'progression_studios_equipment_meta', $_POST['video_equipment']);
        add_post_meta($post_id, 'progression_studios_media_duration', $_POST['search_duration']);
        add_post_meta($post_id, 'progression_studios_slider_btn_link', get_permalink($post_id));
        add_post_meta($post_id, 'progression_studios_trailer_post', $_POST['trailer_url']);
        add_post_meta($post_id, 'progression_studios_video_post', $_POST['video_url']);
        add_post_meta($post_id, 'progression_studios_slider_header_image', $_POST['thumbnail_url']);
        add_post_meta($post_id, 'progression_studios_header_image', $_POST['thumbnail_url']);
        add_post_meta($post_id, 'progression_studios_slider_header_image_id', get_image_id($_POST['thumbnail_url']));
        add_post_meta($post_id, 'progression_studios_header_image_id', get_image_id($_POST['thumbnail_url']));
        add_post_meta($post_id, 'progression_studios_video_embed_poster', $_POST['poster_url']);
        add_post_meta($post_id, 'progression_studios_poster_image', $_POST['poster_url']);
        add_post_meta($post_id, 'progression_studios_poster_image_id', get_image_id($_POST['poster_url']));
        add_post_meta($post_id, 'progression_studios_video_embed_poster_id', get_image_id($_POST['poster_url']));
        add_post_meta($post_id, '_thumbnail_id', get_image_id($_POST['poster_url']));
        add_post_meta($post_id, 'post_instructor', wp_get_current_user()->display_name);
        add_post_meta($post_id, 'access_plan', $plan_id);
        foreach($_POST as $key => $value) {
            if (strpos($key, 'type-') !== false) {
                array_push($type_array, get_term_by('slug', $value, 'video-type')->term_id);
                wp_set_object_terms($post_id, $type_array, 'video-type');
            } else if (strpos($key, 'genre-') !== false) {
                array_push($genres_array, get_term_by('slug', $value, 'video-genres')->term_id);
                wp_set_object_terms($post_id, $genres_array, 'video-genres');
            }
        }
        wp_set_object_terms($post_id, $tax_id, 'video-cast');

        $plan_options = array();
        $plan_options['access_type'] = 'lifetime';
        $plan_options['payment_type'] = 'one_time';
        $plan_options['pricetext'] = $_POST['title'];


        $post_data_array = array(
            'arm_subscription_plan_name' => $_POST['title'],
            'arm_subscription_plan_type' => 'paid_finite',
            'arm_subscription_plan_options' => maybe_serialize( $plan_options ),
            'arm_subscription_plan_amount' => $_POST['plan_amount'],
            'arm_subscription_plan_status' => 1,
            'arm_subscription_plan_role'    => 'armember',
            'arm_subscription_plan_post_id' => $post_id,
            'arm_subscription_plan_is_delete' => 0,
            'arm_subscription_plan_created_date' => current_time('mysql'),
        );

        $wpdb->insert(
            $ARMember->tbl_arm_subscription_plans,
            $post_data_array
        );

        redirect('/exercitii');
     }
}

add_action( 'init', 'save_video' );
function save_video()
{
    if($_REQUEST['save_video_btn'] == 'SALVEAZĂ VIDEO') {
        $post_arr = array(
            'ID'            => $_POST['post_id'],
            'post_title'    => $_POST['title'],
            'post_content'  => $_POST['description'],
            'post_status'   => 'publish',
            'post_type'     => 'video_skrn',
            'ping_status'   => 'closed',
            'comment_status'    => 'closed',
        );
        wp_update_post($post_arr);
        update_post_meta($_POST['post_id'], '_edit_lock', round(microtime(true) * 1000) . ':' . get_current_user_id());
        update_post_meta($_POST['post_id'], '_edit_last', get_current_user_id());
        update_post_meta($_POST['post_id'], 'progression_studios_blog_featured_image_link', 'progression_link_default');
        update_post_meta($_POST['post_id'], 'progression_studios_slider_btn_target', 'link');
        update_post_meta($_POST['post_id'], 'progression_studios_slider_btn_text', 'Play');
        update_post_meta($_POST['post_id'], 'progression_studios_slider_btn_icon', 'fas fa-play');
        update_post_meta($_POST['post_id'], 'progression_studios_media_duration_meta', $_POST['video_duration']);
        update_post_meta($_POST['post_id'], 'progression_studios_equipment_meta', $_POST['video_equipment']);
        update_post_meta($_POST['post_id'], 'progression_studios_media_duration', $_POST['search_duration']);
        update_post_meta($_POST['post_id'], 'progression_studios_slider_btn_link', $_POST['post_id']);
        update_post_meta($_POST['post_id'], 'progression_studios_video_post', $_POST['video_url']);
        update_post_meta($_POST['post_id'], 'progression_studios_trailer_post', $_POST['trailer_url']);
        update_post_meta($_POST['post_id'], 'progression_studios_slider_header_image', $_POST['thumbnail_url']);
        update_post_meta($_POST['post_id'], 'progression_studios_header_image', $_POST['thumbnail_url']);
        update_post_meta($_POST['post_id'], 'progression_studios_slider_header_image_id', get_image_id($_POST['thumbnail_url']));
        update_post_meta($_POST['post_id'], 'progression_studios_header_image_id', get_image_id($_POST['thumbnail_url']));
        update_post_meta($_POST['post_id'], 'progression_studios_video_embed_poster', $_POST['poster_url']);
        update_post_meta($_POST['post_id'], 'progression_studios_poster_image', $_POST['poster_url']);
        update_post_meta($_POST['post_id'], 'progression_studios_poster_image_id', get_image_id($_POST['poster_url']));
        update_post_meta($_POST['post_id'], 'progression_studios_video_embed_poster_id', get_image_id($_POST['poster_url']));
        update_post_meta($_POST['post_id'], '_thumbnail_id', get_image_id($_POST['poster_url']));
        $type_array = [];
        $genres_array = [];
        foreach($_POST as $key => $value) {
            if (strpos($key, 'type-') !== false) {
                array_push($type_array, get_term_by('slug', $value, 'video-type')->term_id);
            } else if (strpos($key, 'genre-') !== false) {
                array_push($genres_array, get_term_by('slug', $value, 'video-genres')->term_id);
            }
        }
        wp_set_object_terms($_POST['post_id'], $type_array, 'video-type');
        wp_set_object_terms($_POST['post_id'], $genres_array, 'video-genres');
        redirect('/instructor-edit');
     }
}

function custom_rewrite_tag() {
    add_rewrite_tag('%post_id%', '([^&]+)');
    add_rewrite_tag('%instructor%', '([^&]+)');
  }
add_action('init', 'custom_rewrite_tag', 10, 0);

add_action( 'init', 'edit_biografie' );
function edit_biografie()
{
    if($_REQUEST['add_video_btn'] == 'SALVEAZĂ' && count(get_posts(['author' => get_current_user_id(), 'post_type' => 'wpstream_product'])) > 0) {
        $term = get_term_by('name', wp_get_current_user()->display_name, 'video-cast');
        if (get_term_meta($term->term_id, 'descriere_instructor') == array() && get_term_meta($term->term_id, 'video_instructor') == array()) {
            add_term_meta($term->term_id, 'descriere_instructor', $_POST['description']);
            add_term_meta($term->term_id, 'video_instructor', $_POST['bio_video_url']);
        } else {
            update_term_meta($term->term_id, 'descriere_instructor', $_POST['description']);
            update_term_meta($term->term_id, 'video_instructor', $_POST['bio_video_url']);
        }
        // redirect('/instructor-edit');
     }
}

add_action( 'init', 'add_channel' );
function add_channel()
{
    if($_REQUEST['add_channel_btn'] == 'ADAUGĂ LIVE') {
        if (count(get_user_meta(get_current_user_id(), 'live_link')) > 0) {
            update_user_meta(get_current_user_id(), 'live_link', $_POST['live_link']);
        } else {
            add_user_meta(get_current_user_id(), 'live_link', $_POST['live_link']);
        };
     }
    if($_REQUEST['delete_channel_btn'] == 'ȘTERGE LIVE') {
        delete_user_meta(get_current_user_id(), 'live_link');
     }
}