<?php
/**
 * The template for displaying Archive pages.
 * Template Name: InstructorEdit
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package progression
 */

	$args = array(
		'author'        =>  get_current_user_id(),
		'post_type'     =>  'video_skrn',
		);

	$posts = get_posts($args);

	$is_authorized = false;
	foreach($current_user->roles as $role) {
		if($role == 'contributor' || $role == 'administrator' || $role == 'admin') {
			$is_authorized = true;
		}
	}
	if(!$is_authorized) {
		wp_redirect(get_home_url());
	}

	get_header(); 
?>
<?php 
$userdata = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); 
?>
	
	<div id="content-sidebar-pro">		
		<div id="content-sidebar-info">
			<div id="avatar-sidebar-large-profile" style="background-image:url('<?php if($userdata->avatar): ?><?php echo esc_url($userdata->avatar);  ?><?php else: ?><?php echo get_avatar_url( $userdata->user_email, 200 ); ?><?php endif; ?>')"></div>

			<div id="profile-sidebar-name">
				<h5><?php if($userdata->first_name): ?>
							<?php echo esc_attr($userdata->first_name); ?> <?php echo esc_attr($userdata->last_name); ?>
						<?php else: ?>
							<?php echo esc_attr($userdata->display_name); ?>
						<?php endif; ?></h5>
				<?php if($userdata->country): ?><h6><?php echo esc_attr($userdata->country); ?></h6><?php endif; ?></h5>
					
					<?php if(function_exists('arm_check_for_wp_rename')  ): ?>
					<?php
					$socialProfileFields = $arm_member_forms->arm_social_profile_field_types();
		         if ($arm_social_feature->isSocialFeature) {
		             if (!empty($socialProfileFields) ) {
						?>	 
							<ul class="profile-social-media-sidebar-icons">
					 
						<?php	 
		            foreach ($socialProfileFields as $spfKey => $spfLabel) {
		                $spfMetaKey = 'arm_social_field_'.$spfKey;
		                $spfMetaValue = get_user_meta($userdata->ID, $spfMetaKey, true);
							 $skey_field = get_user_meta($userdata->ID,$spfMetaKey,true);
						?>
				
				  			 <?php if(!empty($spfMetaValue)): ?>
				  				 <li><a target="_blank" href="<?php echo esc_attr($skey_field);?>"><i class="fab fa-<?php if($spfKey == "googleplush"): ?>google-plus<?php else: ?><?php echo esc_attr($spfKey);?><?php endif; ?>"></i></a></li>
				  				<?php endif; ?>
					<?php } ?>
		 					</ul>
						 <?php
		           }
					}
					?>
					<?php endif; ?>
					
			</div>
			<div id="profile-sidebar-gradient"></div>
			<?php if(get_query_var('author_name')) : $curauth = get_user_by('slug', get_query_var('author_name')); else : $curauth = get_userdata(get_query_var('author')); endif;
				if( $curauth->ID == $user_ID) : ?>
			<?php if(function_exists('arm_check_for_wp_rename')  ): 
				global $arm_global_settings;
				$global_settings = $arm_global_settings->global_settings;
			?>
			<a href="<?php echo esc_url( $arm_global_settings->arm_get_permalink('', $global_settings['edit_profile_page_id']) ); ?>" class="edit-profile-sidebar"><i class="fas fa-pencil-alt"></i></a>
			<?php endif; ?>
			<?php endif; ?>
		</div>
		
		
		<?php

		$fav_videos_loop = new WP_Query( apply_filters( 'widprogression_get_favorite_user_videos', array(
			'posts_per_page'      => 99999,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true,
			'favorite_videos'    => $userdata->ID,
			'post_type'           => 'video_skrn'
		) ) );
			
		$wishlist_videos_loop = new WP_Query( apply_filters( 'widprogression_wishlist_videos_user_videos', array(
			'posts_per_page'      => 99999,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true,
			'wishlist_videos'    => $userdata->ID,
			'post_type'           => 'video_skrn'
		) ) );
		
		
	
			
		?>

		<?php
		$reiew_count_args = array(
		    'user_id' => $userdata->ID, // comments by this user only
		    'status' => 'approve',
		    'post_status' => 'publish',
		    'post_type' => 'video_skrn'
		);
		
		?>
		
		<?php if (get_theme_mod( 'progression_studios_profile_page_user_stats', 'true') == 'true') : ?>
		<div class="content-sidebar-section">
			<h3 class="content-sidebar-sub-header"><?php esc_html_e( 'User Stats', 'skrn-progression' ); ?></h3>
			<ul id="profile-watched-stats">
				<li><span><?php echo esc_attr($fav_videos_loop->post_count); ?></span> <?php esc_html_e( 'Favorites', 'skrn-progression' ); ?></li>
				<li><span><?php echo esc_attr($wishlist_videos_loop->post_count); ?></span> <?php esc_html_e( 'Watchlist', 'skrn-progression' ); ?></li>
				<li><span><?php echo skrn_custom_count_post_by_author($reiew_count_args); ?></span> <?php esc_html_e( 'Ratings', 'skrn-progression' ); ?></li>
			</ul>
		</div><!-- close .content-sidebar-section -->
		<?php endif; ?>
		
		
		<?php if(function_exists('arm_check_for_wp_rename')  ): 
		$date_format = $arm_global_settings->arm_get_wp_date_format();
		?>

			<?php if (get_theme_mod( 'progression_studios_profile_page_member_since', 'true') == 'true') : ?>
			<div class="content-sidebar-section">
				<h3 class="content-sidebar-sub-header"><?php esc_html_e( 'Member Since', 'skrn-progression' ); ?></h3>
				<div class="content-sidebar-simple-text">
					<?php echo date_i18n($date_format, strtotime($userdata->user_registered));?>
				</div>
			</div><!-- close .content-sidebar-section -->
			<a href="/add-video/" class="editeaza-instructor-button">ADD VIDEO</a>
			<?php endif; ?>
			
			<?php if (get_theme_mod( 'progression_studios_profile_page_biography', 'true') == 'true') : ?>
			<?php if($userdata->description): ?>
			<div class="content-sidebar-section">
				<h3 class="content-sidebar-sub-header"><?php esc_html_e( 'Biography', 'skrn-progression' ); ?></h3>
				<div class="content-sidebar-simple-text">
					<?php echo esc_attr($userdata->description); ?>
				</div>
			</div><!-- close .content-sidebar-section -->
			<?php endif; ?>
			<?php endif; ?>
			
		<?php endif; ?><!-- end ARMember Actionvation Check -->

		
	</div><!-- close #content-sidebar-pro -->
	
	
	<div id="col-main-with-sidebar">
		
		<div class="dashboard-container">
			
			
			
			
			<div id="skrn-progression-watchlist-div-container">
				<ul class="dashboard-sub-menu">
					<li class="current"><a href="/instructor-edit/"><?php esc_html_e( 'My videos', 'skrn-progression' ); ?></a></li>
					<?php if (get_theme_mod( 'progression_studios_profile_page_favorites', 'true') == 'true') : ?><li><a href="/editeaza-biografie/"><?php esc_html_e( 'Edit biography', 'skrn-progression' ); ?></a></li><?php endif; ?>
					<?php if (get_theme_mod( 'progression_studios_profile_page_favorites', 'true') == 'true') : ?><li><a href="/live-stream-instructor/"><?php esc_html_e( 'Live stream coach', 'skrn-progression' ); ?></a></li><?php endif; ?>
				</ul>



				<div class="container-fluid">
					<div class="row edit-video-row">
						<?php foreach ($posts as $post): ?>
							<?php $post_link = get_permalink($post->ID); ?>
							<div class="col-lg-3 col-xs-12 col-sm-12 video-cols">
								<div class="edit-instrutor-img">
									<a href="<?php echo $post_link; ?>"><img src="<?php echo  get_post_meta($post->ID, 'progression_studios_header_image')[0] ?>"></a>
								</div>
								<div class="title-button">
										<a href="<?php echo $post_link; ?>"><h3><?php the_title(); ?></h3></a>
										<a href="/editare-video/?post_id=<?php echo $post->ID ?>" class="edit-video-breadchrumb">Edit video</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>

			</div><!-- close #skrn-progression-watchlist-div-container -->




			<?php
			
			?>
			
			
			
			
		</div><!-- close .dashboard-container -->
	</div>
	

<?php get_footer(); ?>